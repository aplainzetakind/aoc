module Main where

import Advent
import Options.Applicative
import Data.Foldable
import Text.Printf
import qualified Data.Text.Lazy.IO as T

data Day = Day Int | All deriving Show

data Options = Options { getDays :: Day
                       , getYear :: Int
                       , getStar :: Stars
                       , getBig  :: Bool } deriving Show

readDay :: String -> Maybe Day
readDay s = let d = (read s :: Int)
            in if d > 0 && d < 26 then Just (Day d) else Nothing

readYear :: String -> Maybe Int
readYear s = let y = (read s :: Int)
             in if y > 2014 && y < 2021 then Just y else Nothing

readStar :: String -> Maybe Stars
readStar s = case s of
              "1" -> Just First
              "2" -> Just Second
              _   -> Nothing

options :: Parser Options
options = Options
          <$> option (maybeReader readDay)
              ( short 'd'
              <> help "Day number. Runs all days if omitted."
              <> showDefault
              <> value All
              <> metavar "N" )
          <*> option (maybeReader readYear)
              ( short 'y'
              <> help "Event year. Runs latest if omitted."
              <> showDefault
              <> value 2020
              <> metavar "N" )
          <*> option (maybeReader readStar)
              ( short 's'
              <> help "Star number. Runs both if omitted."
              <> showDefault
              <> value Both
              <> metavar "N" )
          <*> switch ( short 'b'
              <> help "Use big inputs if available." )

runOne :: Stars -> Bool -> Int -> Int -> IO ()
runOne s b y d = do let filename = if b then "biginput" else "input"
                        path     = printf "inputs/Y%d/day%02d/%s" y d filename
                    (s1, s2) <- advent y d <$> T.readFile path
                    putStrLn $ printf "AoC %d day %02d" y d
                    let star1 = putStrLn $ printf "Silver: %s" s1
                        star2 = putStrLn $ printf "Gold  : %s" s2
                    case s of
                      First  -> star1
                      Second -> star2
                      Both   -> star1 >> star2


run :: Options -> IO ()
run (Options d y s b)
        = if null (dayList d)
            then fail "Day not available."
            else traverse_ (runOne s b y) $ dayList d
           where dayList = \case
                              Day n -> [n]
                              All -> [1..25]

main :: IO ()
main = run =<< execParser opts
  where
    opts = info (options <**> helper)
      ( fullDesc
     <> progDesc "Main executable for solutions to AoC"
     <> header "Advent of Code" )
