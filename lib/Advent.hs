{-# LANGUAGE CPP #-}
module Advent (advent, Stars(..), InputStream) where

import Advent.Utils (Stars(..), InputStream)


#ifdef YEAR2015
import qualified Advent.Y2015 as Y2015
#endif


#ifdef YEAR2016
import qualified Advent.Y2016 as Y2016
#endif


#ifdef YEAR2017
import qualified Advent.Y2017 as Y2017
#endif


#ifdef YEAR2018
import qualified Advent.Y2018 as Y2018
#endif


#ifdef YEAR2019
import qualified Advent.Y2019 as Y2019
#endif


#ifdef YEAR2020
import qualified Advent.Y2020 as Y2020
#endif

advent :: InputStream s => Int -> Int -> s -> (String, String)
advent y = case y of
#ifdef YEAR2015
           2015 -> Y2015.advent
#endif
#ifdef YEAR2016
           2016 -> Y2016.advent
#endif
#ifdef YEAR2017
           2017 -> Y2017.advent
#endif
#ifdef YEAR2018
           2018 -> Y2018.advent
#endif
#ifdef YEAR2019
           2019 -> Y2019.advent
#endif
#ifdef YEAR2020
           2020 -> Y2020.advent
#endif
           yr   -> error $ "Not compiled with " ++ show yr ++ "."
