module Advent.Y2020.Day23 where

import Advent.Utils
import Data.Array.MArray
import Data.Array.ST
import Control.Monad.ST
import Data.Foldable

type ParsedInput = [Int]

parseInput :: forall s. InputStream s => s -> ParsedInput
parseInput s = case parse (many (digitChar :: Parser s Char)) "" s of
                 Right cs -> fmap  (read . pure) cs
                 _       -> error "Parse failed."

makeArray :: Int -> [Int] -> ST s (STUArray s Int Int)
makeArray s z@(x : xs) = do arr <- newArray_ (1, s)
                            traverse_ (uncurry (writeArray arr))
                                                 $ zip (x : xs) xs
                            let m = length z
                                y = last xs
                            writeArray arr y $ if m == s then x else m + 1
                            traverse_ (\n -> writeArray arr n (n + 1))
                                      [m + 1..s - 1]
                            writeArray arr (if s > m then s else y) x
                            pure arr
makeArray _ _ = error "Empty input."

times :: Monad m => Int -> (a -> m a) -> a -> m [a]
times 0 _ _ = pure []
times n f a = do a0 <- f a
                 (a0 :) <$> times (n - 1) f a0

pred' :: Int -> Int -> Int
pred' b 1 = b
pred' _ n = pred n

findNot :: Int -> Int -> Int -> Int -> Int -> Int
findNot b x y z n = if n /= x && n /= y && n /= z
                    then n else findNot b x y z $ pred' b n

move :: Int -> STUArray s Int Int -> ST s Int
move n arr = do (_, b) <- getBounds arr
                x <- readArray arr n
                y <- readArray arr x
                z <- readArray arr y
                w <- readArray arr z
                let t = findNot b x y z $ pred' b n
                t' <- readArray arr t
                writeArray arr n w
                writeArray arr t x
                writeArray arr z t'
                pure w

run :: [Int] -> Int {- ^ bound -}
               -> Int {- ^ moves -}
               -> ST s [Int]
run xs@(x : _) b k = do arr <- makeArray b xs
                        let go 0 _ = pure ()
                            go n y = move y arr >>= go (n - 1)
                        go k x
                        times 8 (readArray arr) 1
run _ _ _ = error "Empty input."

star1 :: ParsedInput -> String
star1 i = let xs = runST $ run i 9 100
          in xs >>= show

star2 :: ParsedInput -> Int
star2 i = let xs = runST $ run i 1000000 10000000
          in product $ take 2 xs

solve :: Solver ParsedInput String Int
solve i = (star1 i, star2 i)
