module Advent.Y2020.Day01 where

import Advent.Utils
import Data.List (sort)

type ParsedInput = [Int]

parseInput :: InputStream s => s -> ParsedInput
parseInput s = case parse (many intLine <* eof) "" s of
                    Right ns -> ns
                    Left _   -> error "Parse failed."

intLine :: InputStream s => Parser s Int
intLine = decimal <* eol

find2 :: Int -> [Int] -> [Int]
find2 n xs = let xs' = sort xs
             in go xs' (reverse xs')
       where go a@(y : ys) b@(z : zs) = case (y + z) `compare` n of
                                          LT -> go ys b
                                          GT -> go a zs
                                          EQ -> [y, z]
             go _          _          = []

find3 :: Int -> [Int] -> [Int]
find3 _ []       = []
find3 n (x : xs) = case find2 (n - x) xs of
                     [] -> find3 n xs
                     ys  -> x : ys

star1 :: [Int] -> Int
star1 = product . find2 2020

star2 :: [Int] -> Int
star2 = product . find3 2020

solve :: Solver ParsedInput Int Int
solve i = (star1 i, star2 i)
