module Advent.Y2020.Day02 where

import Advent.Utils
import Control.Arrow ((&&&))
import Data.Char

type ParsedInput s = [(Rule, s)]

type Rule = ((Int, Int), Char)

parseInput :: InputStream s => s -> [(Rule, s)]
parseInput s = case parse (many parseLine <* eof) "" s of
                 Right r -> r
                 Left _  -> error "Parse failed."

parseLine :: InputStream s => Parser s (Rule, s)
parseLine = do n1 <- decimal
               _ <- char '-'
               n2 <- decimal
               _ <- char ' '
               c <- lowerChar
               _ <- char ':' >> char ' '
               s <- takeWhileP' isLower
               _ <- eol
               pure (((n1, n2), c), s)

isValid :: InputStream s => Rule -> s -> Bool
isValid ((n1, n2), c) = uncurry (&&)
                        . ((>= fromIntegral n1) &&& (<= fromIntegral n2))
                        . length . filter (== c) . chunkToTokens'

isValid2 :: InputStream s => Rule -> s -> Bool
isValid2 ((n1, n2), c) = (== 1) . length . filter (== c)
                         . (\s -> (s !!) <$>  [ fromIntegral n1 - 1
                                                 , fromIntegral n2 - 1])
                         . chunkToTokens'

star1 :: InputStream s => [(Rule, s)] -> Int
star1 = length . filter (uncurry isValid . fmap chunkToTokens')

star2 :: InputStream s => [(Rule, s)] -> Int
star2 = length . filter (uncurry isValid2. fmap chunkToTokens')

solve :: InputStream s => Solver (ParsedInput s) Int Int
solve i = (star1 i, star2 i)
