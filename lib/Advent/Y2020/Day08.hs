module Advent.Y2020.Day08 where

import Advent.Utils
import Data.Array.IArray
import qualified Data.Set as S
import Data.Either
import Control.Monad

type ParsedInput = Program

type Program = Array Int Instruction

data Instruction = ACC Int
                 | JMP Int
                 | NOP Int

parseInput :: InputStream s => s -> ParsedInput
parseInput s = case parse (many $ instruction <* eol) "" s of
                  Right is -> (\xs -> listArray (0, length xs - 1) xs) is
                  Left _   -> error "Parse failed."

instruction :: InputStream s => Parser s Instruction
instruction = do c <- replicateM 3 letterChar
                 hspace
                 n <- signed hspace decimal
                 case c of
                   "acc" -> pure $ ACC n
                   "jmp" -> pure $ JMP n
                   "nop" -> pure $ NOP n
                   _     -> fail "Can't parse instruction."

nextIP :: Instruction -> Int -> Int
nextIP (JMP n) = (+ n)
nextIP _       = (+ 1)

nextAcc :: Instruction -> Int -> Int
nextAcc (ACC n) = (+ n)
nextAcc _       = id

-- This assumes that none of the substitutions terminate by jumping at an
-- out-of-bounds position other than the one after the last.
runDetectingLoop :: Int -> Int -> S.Set Int -> Program -> Either Int Int
runDetectingLoop a p s m
        = let i  = m ! p
              p' = nextIP  i p
              a' = nextAcc i a
              s' = p' `S.insert` s
          in if p' `S.member` s
                then Left a'
                else if bounds m `inRange` p'
                        then runDetectingLoop a' p' s' m
                        else Right a'

runProg :: Program -> Either Int Int
runProg = runDetectingLoop 0 0 S.empty

star1 :: Program -> Int
star1 arr = case runProg arr of
                Left n  -> n
                Right _ -> error "No loop."

allSubstitutions :: Program -> [Program]
allSubstitutions arr = let (b, c) = bounds arr
                       in do i <- [b..c]
                             case arr ! i of
                               JMP n -> pure $ arr // [(i, NOP n)]
                               NOP n -> pure $ arr // [(i, JMP n)]
                               _     -> []

star2 :: Program -> Int
star2 arr = case filter isRight $ runProg <$> allSubstitutions arr of
              (Right n : _) -> n
              _             -> error "No termination."

solve :: Solver ParsedInput Int Int
solve i = (star1 i, star2 i)
