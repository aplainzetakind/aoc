module Advent.Y2020.Day15 where

import Advent.Utils
import Data.Array.ST
import qualified Data.Array.MArray as MA
import Control.Monad.ST
import Data.Word
import Data.Foldable

type ParsedInput = [Word32]

parseInput :: forall s. InputStream s => s -> ParsedInput
parseInput s = case parse (decimal `sepBy` single ',' <* eol
                           :: Parser s [Word32]) "" s of
                 Right i -> i
                 _       -> error "Parse failed."


mx :: Word32
mx = maxBound

processList :: [Word32] -> STUArray s Word32 Word32 -> ST s (Word32, Word32)
processList xs arr = do traverse_ (uncurry (MA.writeArray arr))
                           $ zip (init xs) [1..]
                        let n = fromIntegral $ length xs
                            l = last xs
                        pure (n, l)

findKth :: Word32 -> ParsedInput -> Word32
findKth k xs
    = runST $ do arr <- MA.newArray (0,k) mx
                 (lastturn, lastnum) <- processList xs arr
                 let go j l | j == k = pure l
                     go n l = do t <- MA.readArray arr l
                                 let i = if t == mx then 0 else n - t
                                 MA.writeArray arr l n >> go (n + 1) i
                 go lastturn lastnum

star1 :: ParsedInput -> Word32
star1 = findKth 2020

star2 :: ParsedInput -> Word32
star2 = findKth 30000000

solve :: Solver ParsedInput Word32 Word32
solve i = (star1 i, star2 i)
