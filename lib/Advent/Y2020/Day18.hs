module Advent.Y2020.Day18 where

import Advent.Utils
import Data.Either

type ParsedInput s = s

parseInput :: s -> s
parseInput = id

data Expr = Lit Int
           | Add Expr Expr
           | Mul Expr Expr deriving Show

eval :: Expr -> Int
eval (Lit n) = n
eval (Add e e') = eval e + eval e'
eval (Mul e e') = eval e * eval e'

lit :: InputStream s => Parser s Expr
lit = Lit <$> decimal <* hspace

parens :: InputStream s => Parser s a -> Parser s a
parens p = between (single '(') (single ')') p <* hspace

more :: InputStream s => Expr -> Parser s Expr
more e = ((eol <|> lookAhead (string ")")) $> e)
            <|> do c  <- (single '+' <|> single '*') <* hspace
                   e' <- lit <|> parens grammar1
                   let o = case c of
                             '+' -> Add
                             '*' -> Mul
                             _   -> error "Impossible."
                   more $ o e e'

grammar1 :: InputStream s => Parser s Expr
grammar1 = (lit <|> parens grammar1) >>= more

star1 :: InputStream s => ParsedInput s -> Int
star1 = sum . fmap eval
        . fromRight (error "Parse failed.")
        . parse (many grammar1) ""

grammar2 :: InputStream s => Parser s Expr
grammar2 = (try mul <|> try add <|> try lit <|> parens grammar2) <* hspace

add :: InputStream s => Parser s Expr
add = do s1 <- try lit <|> parens grammar2
         single '+' *> hspace $> ()
         s2 <- try add <|> lit <|> parens grammar2
         pure $ Add s1 s2

mul :: InputStream s => Parser s Expr
mul = do s1 <- try add <|> lit <|> parens grammar2
         single '*' *> hspace $> ()
         s2 <- try mul <|> try add <|> lit <|> parens grammar2
         pure $ Mul s1 s2

star2 :: InputStream s => ParsedInput s -> Int
star2 = sum . fmap eval
        . fromRight (error "Parse failed.")
        . parse (many (grammar2 <* eol)) ""

solve :: InputStream s => Solver (ParsedInput s) Int Int
solve i = (star1 i, star2 i)
