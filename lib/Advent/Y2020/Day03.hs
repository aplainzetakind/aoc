module Advent.Y2020.Day03 where

import Advent.Utils
import Data.Char

type ParsedInput s = [s]

parseInput :: InputStream s => s -> [s]
parseInput s = case parse (many stringLine <* eof) "" s of
                 Right ls -> ls
                 Left _   -> error "Parse failed."

stringLine :: InputStream s => Parser s s
stringLine = takeWhileP' isPrint <* eol

filterDowns :: Int -> [a] -> [a]
filterDowns m (x : xs) = x : filterDowns m (drop (m - 1) xs)
filterDowns _ [] = []

slope :: InputStream s => Int -> Int -> [s] -> Int
slope n m s' = let s = fmap chunkToTokens' s'
                   len = length . head $ s
               in length . filter (== '#')
                  . fmap head
                  . zipWith drop ((`mod` len) <$> [0, fromIntegral n..])
                  . filterDowns m $ s

star1 :: InputStream s => [s] -> Int
star1 = slope 3 1

star2 :: InputStream s => [s] -> Int
star2 s = product . fmap ($ s) $ zipWith slope [1, 3, 5, 7, 1] [1,1,1,1,2]

solve :: InputStream s => Solver (ParsedInput s) Int Int
solve i = (star1 i, star2 i)
