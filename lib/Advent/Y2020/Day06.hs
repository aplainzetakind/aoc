module Advent.Y2020.Day06 where

import Prelude hiding (lines)
import Advent.Utils
import qualified Data.Set as S
import Data.Char

type ParsedInput s = [[s]]

parseInput :: InputStream s => s -> ParsedInput s
parseInput s = case parse (many lines) "" s of
                  Right is -> is
                  Left _   -> error "Parse failed."

groupEnd :: InputStream s => Parser s ()
groupEnd = void eol <|> eof

lines :: InputStream s => Parser s [s]
lines = someTill line groupEnd

line :: InputStream s => Parser s s
line = takeWhileP Nothing isPrint <* eol

accumGroup :: InputStream s => [s] -> [Int]
accumGroup ss = let (xs : xss) = fmap (S.fromList . chunkToTokens') ss
                in (\(s, s') -> [S.size s, S.size s'])
                            $ foldl' (\(s1, s2) cs
                                   -> ( S.union        s1 cs
                                      , S.intersection s2 cs ))
                                            (xs, xs) xss

starBoth :: InputStream s => [[s]] -> [Int]
starBoth = foldl' (zipWith (+)) [0,0] . fmap accumGroup

solve :: InputStream s => Solver (ParsedInput s) Int Int
solve = (\[x, y] -> (x, y)) . starBoth
