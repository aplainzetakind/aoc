module Advent.Y2020.Day10 where

import Advent.Utils
import Data.List

type ParsedInput = [Int]

parseInput :: InputStream s => s -> ParsedInput
parseInput s = case parse (many ((decimal :: InputStream s => Parser s Int)
                                  <* eol)) "" s of
                  Right is -> is
                  Left _   -> error "Parse failed."

f :: (Int, Int, Int) -> Int -> (Int, Int, Int)
f (a, b, c) d = case d - c of
                  1 -> (a + 1, b, d)
                  3 -> (a, b + 1, d)
                  2 -> (a, b, d)
                  _ -> error "unexpected input"

star1 :: [Int] -> Int
star1 = (\(a, b, _) -> a * (b + 1)) . foldl' f (0, 0, 0) . sort

star2 :: [Int] -> Int
star2 ns'
    = let (n1 : n2 : n3 : ns) = sort ns'
          ini = ((n1, v1), (n2, v2), (n3, v3))
          v1 = if n1 <= 3 then 1 else 0
          v2 | n2 <= 3      = 1 + v1
             | n2 <= n1 + 3 =     v1
             | otherwise    = error "Jump too big."
          v3 | n3 <= 3      = 1 + v1 + v2
             | n3 <= n1 + 3 =     v1 + v2
             | n3 <= n2 + 3 =          v2
             | otherwise    = error "Jump too big."
          g ((m1, w1), (m2, w2), (m3, w3)) n
             | m1 >= n - 3 = ((m2, w2), (m3, w3), (n, w1 + w2 + w3))
             | m2 >= n - 3 = ((m2, w2), (m3, w3), (n,      w2 + w3))
             | m3 >= n - 3 = ((m2, w2), (m3, w3), (n,           w3))
             | otherwise    = error "Jump too big."
      in (\(_, _, (_, x)) -> x) $ foldl' g ini ns

solve :: Solver ParsedInput Int Int
solve i = (star1 i, star2 i)
