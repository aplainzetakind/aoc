module Advent.Y2020.Day04 where

import Advent.Utils
import Data.Char

type ParsedInput s = [[(s, s)]]

parseInput :: InputStream s => s -> ParsedInput s
parseInput s = case parse (many fields) "" s of
                  Right is -> is
                  Left _   -> error "Parse failed."

groupEnd :: InputStream s => Parser s ()
groupEnd = void eol <|> eof

fields :: InputStream s => Parser s [(s, s)]
fields = someTill field groupEnd

field :: InputStream s => Parser s (s, s)
field = do f <- takeWhileP' isLetter
           _ <- char ':'
           v <- takeWhileP' (\c -> isAlphaNum c || c == '#')
           void eol <|> try hspace
           pure (f, v)

isValid :: InputStream s => (s, String) -> Bool
isValid ("byr", f) = length f == 4
                     && let y = (read f :: Int) in y >= 1920 && y <= 2002
isValid ("iyr", f) = length f == 4
                     && let y = (read f :: Int) in y >= 2010 && y <= 2020
isValid ("eyr", f) = length f == 4
                     && let y = (read f :: Int) in y >= 2020 && y <= 2030
isValid ("hgt", f) = let (num, un) = (read $ takeWhile isDigit f :: Int
                                     , dropWhile isDigit f)
                     in case un of
                         "cm" -> num >= 150 && num <= 193
                         "in" -> num >= 56  && num <= 76
                         _    -> False
isValid ("hcl", f) = case f of
                       ('#' : ds) -> length ds == 6
                                        && all ( `elem` (['a'..'f']
                                                         ++ ['0'..'9']) ) ds
                       _            -> False
isValid ("ecl", f) = f `elem` ["amb", "blu", "brn", "gry", "grn", "hzl", "oth" ]
isValid ("pid", f) = length f == 9 && all isDigit f
isValid _          = False

star1 :: InputStream s => [[(s, s)]] -> Int
star1 = length
        . filter ((== 7) . length)
        . fmap (filter (/= "cid") . fmap fst)

star2 :: InputStream s => [[(s, s)]] -> Int
star2 = length
        . filter (all isValid . fmap (fmap chunkToTokens'))
        . filter ((== 7) . length)
        . fmap (filter ((/= "cid") . fst))

solve :: InputStream s => Solver (ParsedInput s) Int Int
solve i = (star1 i, star2 i)
