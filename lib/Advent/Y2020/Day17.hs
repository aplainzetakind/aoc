module Advent.Y2020.Day17 where

import Advent.Utils
import Control.Monad
import Control.Monad.State
import qualified Data.Vector as V
import qualified Data.Set as S
import qualified Data.Map.Strict as M
import Data.Map.Internal (keysSet)

type ParsedInput = [[Bool]]

parseInput :: InputStream s => s -> ParsedInput
parseInput = fmap (fmap (== '#')) . lines . chunkToTokens'

type Point3 = (Int, Int, Int)
type Point4 = (Int, Int, Int, Int)

type Space a = State LifeState a

data LifeState
        = LifeState { neighbourMap :: !(M.Map (V.Vector Int) [V.Vector Int])
                    , activeCells  :: !(S.Set (V.Vector Int)) }

neighbours :: V.Vector Int -> [V.Vector Int]
neighbours v = let (f, r) = V.splitAt 2 v
                   f' = traverse (\n -> [n - 1..n + 1]) f
                   r' = traverse (\n -> filter (>= 0) [n - 1..n + 1]) r
               in (V.++) <$> f' <*> r'

boundaryMultiplicity :: V.Vector Int -> V.Vector Int -> Int
boundaryMultiplicity v w = 2 ^ n
          where n = length . V.filter (== (1, 0)) . V.drop 2 $ V.zip v w

getNeighbours :: V.Vector Int -> Space [(V.Vector Int, Int)]
getNeighbours v
      = do m <- gets neighbourMap
           case M.lookup v m of
             Nothing -> let ns = neighbours v
                            m' = M.insert v ns m
                        in do as <- gets activeCells
                              put (LifeState m' as)
                              pure $ (\n -> (n, boundaryMultiplicity v n))
                                     <$> ns
             Just vs -> pure $ (\n -> (n, boundaryMultiplicity v n)) <$> vs

initSet :: Int -> ParsedInput -> LifeState
initSet k _ | k < 2 = error "Dimension should be at least 2."
initSet k bss = let h  = length bss
                    w  = length $ head bss
                    is = [ V.fromList [r, c] V.++ V.replicate (k - 2) 0
                                           | c <- [0..w - 1]
                                           , r <- [0..h - 1]
                                           , (bss !! r) !! c ]
                in LifeState M.empty (S.fromList is)

step :: Space ()
step = do LifeState nm as <- get
          vs <- concat
                <$> traverse getNeighbours (S.toList as)
          let m = foldl' (\mp (p, n) -> M.insertWith (+) p n mp) M.empty vs
              s = keysSet $ M.filterWithKey f m
              f k v = case (k `S.member` as, v) of
                        (_   ,  3) -> True
                        (True,  4) -> True
                        _          -> False
          put $ LifeState nm s

cyclesInDims :: Int -> Int -> ParsedInput -> Int
cyclesInDims k d i = sum . fmap f . S.toList . activeCells
                     $ (`execState` initSet d i) (replicateM_ k step)
              where f = (2 ^) . V.length . V.filter (/= 0) . V.drop 2

star1 :: ParsedInput -> Int
star1 = cyclesInDims 6 3

star2 :: ParsedInput -> Int
star2 = cyclesInDims 6 4

solve :: Solver ParsedInput Int Int
solve i = (star1 i, star2 i)
