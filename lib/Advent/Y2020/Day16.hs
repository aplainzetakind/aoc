module Advent.Y2020.Day16 where

import Advent.Utils
import Data.Ix
import Control.Monad.Reader
import qualified Data.OrdPSQ as PSQ
import qualified Data.Set as S
import qualified Data.Map as M

data ParsedInput s = Input { fields   :: [Field s]
                           , myTicket :: [Int]
                           , tickets  :: [[Int]] } deriving (Eq, Ord, Show)

data Field s = Field { name :: s, ranges :: [(Int, Int)] } deriving (Eq, Ord, Show)

parseInput :: InputStream s => s -> ParsedInput s
parseInput s = case parse input "" s of
                Right i -> i
                _       -> error "Parse failed."

input :: InputStream s => Parser s (ParsedInput s)
input = do fs <- many field
           eol >> string "your ticket:" >> eol $> ()
           t <- ticket
           eol >> string "nearby tickets:" >> eol $> ()
           ts <- some ticket
           pure $ Input fs t ts

field :: InputStream s => Parser s (Field s)
field = do f <- takeWhileP' (`notElem` (":\n" :: String)) <* string ": "
           a1 <- decimal <* single '-'
           a2 <- decimal <* string " or "
           b1 <- decimal <* single '-'
           b2 <- decimal <* eol
           pure $ Field f [(a1, a2), (b1, b2)]

ticket :: InputStream s => Parser s [Int]
ticket = decimal `sepBy1` single ',' <* eol

star1 :: ParsedInput s -> Int
star1 i = sum . filter nevervalid $ ns
           where ns = concat $ tickets i
                 fs = fields i
                 nevervalid n = not $ any (matches n) fs

type Ticket s a = Reader (ParsedInput s) a

validTickets :: InputStream s => Ticket s [[Int]]
validTickets = do nss <- asks tickets
                  fs  <- asks fields
                  pure $ filter (all (\n -> any (matches n) fs)) nss

matches :: Int -> Field s -> Bool
matches n = any (`inRange` n) . ranges

star2R :: InputStream s => Ticket s Int
star2R = do fs  <- asks fields
            fieldsfilteredatposs
                <- foldl (zipWith (\gs n -> filter (matches n) gs))
                         (repeat fs)
                           <$> validTickets
            let posntaggednamess = zip [0..] $ S.fromList . fmap name
                                               <$> fieldsfilteredatposs
                mp = findPoss
                     . foldl' (\q (n, s) -> PSQ.insert s (S.size s) n q)
                              PSQ.empty
                     $ posntaggednamess
                poss' = M.filterWithKey
                         (\k _ -> (== "departure")
                                  . head
                                  . words
                                  . chunkToTokens' $ k) mp
            tckt <- asks myTicket
            pure $ product . fmap (tckt !!) $ poss'

findPoss :: Ord a => PSQ.OrdPSQ (S.Set a) Int c -> M.Map a c
findPoss = go M.empty S.empty
    where go m _ q | PSQ.null q = m
          go m s q = case PSQ.minView q of
                      Just (ns, _, v, q')
                         -> let ns' = ns S.\\ s
                                k   = S.size ns'
                            in if k == 1 then let n = S.findMin ns'
                                              in go (M.insert n v m)
                                                    (S.insert n s)
                                                    q'
                                         else go m s $  PSQ.insert ns' k v q'
                      _  -> error "Queue exhausted."

star2 :: InputStream s => ParsedInput s -> Int
star2 = runReader star2R

solve :: InputStream s => Solver (ParsedInput s) Int Int
solve i = (star1 i, star2 i)
