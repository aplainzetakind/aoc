module Advent.Y2020.Day19 where

import Advent.Utils hiding (match)
import qualified Data.IntMap as M
import Data.Char (isPrint)

type ParsedInput = (M.IntMap Rule, [String])

data Rule = Ref [[Int]] | Match Char

parseInput :: InputStream s => s -> ParsedInput
parseInput s = case parse input "" s of
                Right (rs, ss) -> (M.fromList rs, ss)
                _              -> error "Parse failed."

input :: InputStream s => Parser s ([(Int, Rule)], [String])
input = do rs <- many rule
           eol $> ()
           ss <- fmap chunkToTokens' <$> many (takeWhileP' isPrint <* eol)
           pure (rs, ss)

rule :: InputStream s => Parser s (Int, Rule)
rule = do n <- decimal <* string ": "
          let refs = some decimal_ `sepBy` string "| "
              ch   = string "\"" *> anySingle <* string "\""
          r <- Match <$> ch <|> Ref <$> refs
          eol $> ()
          pure (n, r)

chain :: [a] -> [[a] -> [a]] -> [a]
chain xs (f : _ ) | null (f xs) = []
chain xs (f : fs) = chain (f xs) fs
chain xs []       = xs

decimal_ :: (InputStream s, Num a) => Parser s a
decimal_ = decimal <* hspace

match :: M.IntMap Rule -> String -> Bool
match m s = ("" `elem`) $ go 0 [s]
   where go ((m M.!) -> Match c) ss
            = drop 1 <$> filter (\case (x : _) -> x == c; _ -> False) ss
         go ((m M.!) -> Ref nss) ss
            = chain ss . fmap go =<< nss

star1 :: ParsedInput -> Int
star1 (m, ss) = length . filter (match m) $ ss

star2 :: ParsedInput -> Int
star2 (m', ss) = length . filter (match m) $ ss
          where m = M.insert 8 (Ref [[42], [42, 8]])
                    . M.insert 11 (Ref [[42, 31], [42, 11, 31]]) $ m'

solve :: Solver ParsedInput Int Int
solve i = (star1 i, star2 i)
