module Advent.Y2020.Day11 where

import Advent.Utils
import Data.Array.MArray
import Data.Array.Unboxed
import Data.Word
import Data.Array.ST
import qualified Data.Map.Strict as M
import Control.Monad.ST
import Data.Foldable
import Data.Maybe

type Point32 = (Word32, Word32)

type ParsedInput = ( M.Map Point32 Word32 -- ^ Map (seat coord.) (seat num.)
                   , Point32 -- ^ (height, witdh)
                   , Word32 -- ^ Number of seats.
                   )

parseInput :: InputStream s => s -> ParsedInput
parseInput s = case parse inputParser "" s of
                  Right is -> is
                  Left _   -> error "Parse failed."

-- | Number each seat while consuming input.
inputParser :: InputStream s => Parser s ParsedInput
inputParser = go M.empty 1 1 1 1 1
        where go :: InputStream s => M.Map Point32 Word32 -> Word32 -> Word32
                    -> Word32 -> Word32 -> Word32 -> Parser s ParsedInput
              go m n i j r c
               = finish <|> next
                   where finish = eof $> (m, (c, r - 1), n - 1)
                         next = do x <- anySingle
                                   case x of
                                     'L'  -> go (M.insert (i, j) n m) (n + 1)
                                               (i + 1) j r c
                                     '.'  -> go m n (i + 1) j r c
                                     '\n' -> go m n 1 (j + 1) (j + 1) (i - 1)
                                     _    -> error "Unexpected input."

neighbours :: Word32 -- ^ Number of seats, to create the array.
              -> Point32 -- ^ Bottom-right corner.
              -> M.Map Point32 Word32
              -> Array Word32 [Word32]
neighbours n (r, c) m = array (1, n) $ f <$> (M.keys m :: [Point32])
    where f p@(x, y) = (m M.! p, ) . mapMaybe (`M.lookup` m)
                                   $ [ (a, b) | a <- [x - 1..x + 1]
                                     , b <- [y - 1..y + 1]
                                     , (a, b) /= (x, y)
                                     , a >= 1, a <= r
                                     , b >= 1, b <= c
                                     ]

seen :: Word32 -- ^ Number of seats.
        -> Point32 -- ^ Bottom-right corner.
        -> M.Map Point32 Word32
        -> Array Word32 [Word32] -- ^ Array (origin seat num)
                                 -- ^       ([seat seen from origin seat])
seen n rc m = array (1, n) $ f <$> (M.keys m :: [Point32])
    where f p = (m M.! p, mapMaybe (firstSeen m rc p) dirs')

firstSeen :: M.Map Point32 Word32
             -> Point32 {- ^ Dimensions -}
             -> Point32 {- ^ Origin -}
             -> (Word32 -> Word32, Word32 -> Word32) {-^ Direction -}
             -> Maybe Word32
firstSeen m rc (a, b) (f, g) = safeHead . mapMaybe (`M.lookup` m)
                               . takeWhile (inRange ((1, 1), rc))
                               $ zip (tail [a, f a..]) (tail [b, g b..])

-- | Ugly cumbersome hack to cope with coordinate-decreasing directions with
-- Word32.
dirs' :: [(Word32 -> Word32, Word32 -> Word32)]
dirs' = [ (subtract 1, subtract 1), (id, subtract 1), ((+ 1), subtract 1)
        , (subtract 1,         id),                   ((+ 1),         id)
        , (subtract 1,      (+ 1)), (id,      (+ 1)), ((+ 1),      (+ 1)) ]

run :: Word32 -- ^ Seat vacating threshold.
       -> (Word32 -> Point32
                  -> M.Map Point32 Word32
                  -> Array Word32 [Word32]) -- ^ Neighbour map.
       -> (M.Map Point32 Word32, Point32, Word32)
       -> ST s Word32
run k f (m, (r, c), n)
         = do a     <- newArray (1, n) False :: ST s (STUArray s Word32 Bool)
              let nbs = f n (r, c) m
                  go  = do fs <- getFlips k nbs a
                           if null fs then fromIntegral . length . filter id
                                           <$> getElems a
                                      else traverse_ (uncurry (writeArray a)) fs
                                           >> go
              go

drop' :: (Eq n, Num n) => n -> [a] -> [a]
drop' 0 xs = xs
drop' _ [] = []
drop' n (_ : xs) = drop' (n - 1) xs

getFlips :: Word32 -- ^ Seat vacating threshold.
         -> Array Word32 [Word32] -- ^ Neighbours.
         -> STUArray s Word32 Bool -- ^ Seating state.
         -> ST s [(Word32, Bool)]
getFlips k nbs arr = foldlM f [] (indices nbs)
       where f ps p = do s <- readArray arr p
                         let ns = nbs ! p
                         fns <- filter id <$> traverse (readArray arr) ns
                         if (s && (not . null . drop' (k - 1) $ fns))
                                  || (not s && null fns)
                            then pure $ (p, not s) : ps
                            else pure ps

star1 :: (M.Map Point32 Word32, Point32, Word32) -> Word32
star1 t = runST $ run 4 neighbours t

star2 :: (M.Map Point32 Word32, Point32, Word32) -> Word32
star2 t = runST $ run 5 seen t

solve :: Solver ParsedInput Word32 Word32
solve i = (star1 i, star2 i)
