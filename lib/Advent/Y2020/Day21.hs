module Advent.Y2020.Day21 where

import Advent.Utils
import Data.Char
import Data.List
import Data.Tuple
import qualified Data.Set as S
import qualified Data.Map as M

type ParsedInput s = [([s], [s])]

parseInput :: InputStream s => s -> ParsedInput s
parseInput s = case parse (many line) "" s of
                  Right is -> is
                  Left _   -> error "Parse failed."

line :: InputStream s => Parser s ([s], [s])
line = do ings <- many word
          as   <- allergenList
          eol $> ()
          pure (ings, as)

word :: InputStream s => Parser s s
word = takeWhile1P' isLetter <* hspace1

allergenList :: InputStream s => Parser s [s]
allergenList = do string "(contains " $> ()
                  (takeWhile1P' isLetter `sepBy1` string ", ") <* single ')'

allergenPotentialIngredientMap :: InputStream s =>
                                  ParsedInput s -> M.Map s (S.Set s)
allergenPotentialIngredientMap = M.fromListWith S.intersection
                                 . concatMap ( fmap (fmap S.fromList . swap)
                                               . sequence )

allIngredients :: InputStream s => ParsedInput s -> S.Set s
allIngredients = S.fromList . concatMap fst

intertIngredients :: InputStream s => ParsedInput s -> S.Set s
intertIngredients i = let m = allergenPotentialIngredientMap i
                          s = allIngredients i
                      in foldl' S.difference s (M.elems m)

star1 :: InputStream s => ParsedInput s -> Int
star1 i = let ingrswithmultiplicity = concatMap fst i
          in length $ filter (`S.member` intertIngredients i)
                             ingrswithmultiplicity

reduce :: InputStream s => M.Map s (S.Set s) -> M.Map s s
reduce = go M.empty
    where go mdone mleft | M.null mleft = mdone
          go mdone mleft = let news   = M.map S.findMin
                                        $ M.filter ((== 1) . S.size) mleft
                               remove = S.fromList $ M.elems news
                               newsl  = M.toList news
                           in go (foldl (\m (k, v) -> M.insert k v m)
                                                        mdone newsl)
                                   (M.map (`S.difference` remove)
                                    . M.filter ((/= 1) . S.size) $ mleft)

star2 :: InputStream s => ParsedInput s -> String
star2 inp = let i = intertIngredients inp
                m = allergenPotentialIngredientMap inp
            in intercalate ","
               . fmap chunkToTokens'
               . M.elems
               . reduce
               . M.map (`S.difference` i) $ m

solve :: InputStream s => Solver (ParsedInput s) Int String
solve i = (star1 i, star2 i)
