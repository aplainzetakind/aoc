module Advent.Y2020.Day12 where

import Advent.Utils

type ParsedInput = [Instruction]

parseInput :: InputStream s => s -> ParsedInput
parseInput s = case parse (many parseInstruction) "" s of
                  Right is -> is
                  Left _   -> error "Parse failed."

parseInstruction :: (Stream s, Token s ~ Char) => Parser s Instruction
parseInstruction = do c <- upperChar
                      n <- decimal
                      (eol $> ()) <|> eof
                      pure $ case c of
                               'L' -> Turn $ turnCCW n
                               'R' -> Turn $ turnCW n
                               'E' -> Move R n
                               'N' -> Move U n
                               'W' -> Move L n
                               'S' -> Move D n
                               'F' -> Forward n
                               _   -> error "Unexpected input."

turnCCW, turnCW :: Int -> Int
turnCCW 90  = 1
turnCCW 180 = 2
turnCCW 270 = 3
turnCCW _   = error "Turn not a multiple of 90."
turnCW 90  = 3
turnCW 180 = 2
turnCW 270 = 1
turnCW _   = error "Turn not a multiple of 90."

data ShipState = SS { getPos     :: Point
                    , getPosWP   :: Point
                    , getBearing :: Direction } deriving Show

data Instruction = Turn Int
                 | Forward Int
                 | Move Direction Int deriving Show

execute :: ShipState -> Instruction -> ShipState
execute ss (Turn 0) = ss
execute ss@SS { getBearing = R } (Turn 1) = ss { getBearing = U }
execute ss@SS { getBearing = U } (Turn 1) = ss { getBearing = L }
execute ss@SS { getBearing = L } (Turn 1) = ss { getBearing = D }
execute ss@SS { getBearing = D } (Turn 1) = ss { getBearing = R }
execute ss (Turn n) = (`execute` Turn (n - 1)) $! execute ss (Turn 1)
execute ss@SS {getPos = p } (Move d n) = ss { getPos = movePoint d n p }
execute ss@(SS p _ d) (Forward n) = ss { getPos = movePoint d n p }

execute' :: ShipState -> Instruction -> ShipState
execute' ss (Turn 0) = ss
execute' ss@SS { getPosWP = (x, y) } (Turn 1) = ss { getPosWP = (- y, x) }
execute' ss (Turn n) = (`execute'` Turn (n - 1)) $! execute' ss (Turn 1)
execute' ss@SS {getPosWP = p } (Move d n) = ss { getPosWP = movePoint d n p }
execute' ss@(SS (s1, s2) (w1, w2) _) (Forward n)
              = ss { getPos = (s1 + n * w1, s2 + n * w2) }

star1 :: ParsedInput -> Int
star1 = (\(x, y) -> abs x + abs y) . getPos
        . foldl' execute (SS (0, 0) (0, 0) R)

star2 :: ParsedInput -> Int
star2 = (\(x, y) -> abs x + abs y) . getPos
        . foldl' execute' (SS (0, 0) (10, 1) R)

solve :: Solver ParsedInput Int Int
solve i = (star1 i, star2 i)
