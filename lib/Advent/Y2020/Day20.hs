module Advent.Y2020.Day20 (parseInput, solve) where

import Advent.Utils
import Data.Word
import Data.Bits
import Data.List
import Data.Array
import Data.Maybe
import qualified Data.Map as M
import qualified Data.Set as S
import Control.Monad

type ParsedInput = [(Int, [String])]

side :: Num a => a
side = 12

parseInput :: InputStream s => s -> ParsedInput
parseInput s = case parse input "" s of
                  Right is -> is
                  Left _   -> error "Parse failed."

input :: InputStream s => Parser s ParsedInput
input = some block

block :: InputStream s => Parser s (Int, [String])
block = do n  <- string "Tile " *> decimal <* single ':' <* eol
           ss <- fmap chunkToTokens'
                 <$> many (takeWhile1P' (`elem` ['#','.']) <* eol)
           eol $> ()
           pure (n, ss)

type Edge = Word16 -- left-to-right or top-to-bottom

stringToEdge :: String -> Edge
stringToEdge s = let s' = replicate 6 '.' ++ s
                 in sum
                    $ zipWith (\n c -> if c == '#' then bit n else 0)
                              [15,14..0] s'

flipEdge :: Edge -> Edge
flipEdge e = sum $ (\n -> if testBit e n then bit (9 - n) else 0) <$> [0..9]

type Edge4 = (Edge, Edge, Edge, Edge) -- (right, top, left, bottom)

leftEdge   :: Edge4 -> Edge
leftEdge   (e, _, _, _) = e

topEdge    :: Edge4 -> Edge
topEdge    (_, e, _, _) = e

rightEdge  :: Edge4 -> Edge
rightEdge  (_, _, e, _) = e

bottomEdge :: Edge4 -> Edge
bottomEdge (_, _, _, e) = e

edges :: [String] -> Edge4
edges ss = let t = stringToEdge $ head ss
               b = stringToEdge $ last ss
               sst = transpose ss
               l = stringToEdge $ head sst
               r = stringToEdge $ last sst
           in (r, t, l, b)

data Transformation = Rotate Int | TransposeRotate Int

allTfs :: [Transformation]
allTfs = (Rotate <$> [0, 1, 2, 3]) ++ (TransposeRotate <$> [0, 1, 2, 3])

blockApply :: Transformation -> [String] -> [String]
blockApply (Rotate 1) ss = [ [ ss !! j !! (9 - i) | j <- [0..9] ] | i <- [0..9] ]
blockApply (Rotate n) ss = (!! n) $ iterate (blockApply (Rotate 1)) ss
blockApply (TransposeRotate n) ss = blockApply (Rotate n) . transpose $ ss

edgesApply :: Transformation -> Edge4 -> Edge4
edgesApply (Rotate 1) (r, t, l, b) = (flipEdge b, r, flipEdge t, l)
edgesApply (Rotate n) es = (!! n) $ iterate (edgesApply (Rotate 1)) es
edgesApply (TransposeRotate n) (r, t, l, b)
        = edgesApply (Rotate n) (b, l, t, r)

blockToEdgeOpts :: [String] -> [(Edge4, Transformation)]
blockToEdgeOpts (edges -> e) = (\t -> (edgesApply t e, t)) <$> allTfs

edge4IDMap :: ParsedInput -> M.Map Edge4 (Int, Transformation)
edge4IDMap = M.fromList
             . fmap (\(d, (es, t)) -> (es, (d, t)))
             . concatMap (traverse blockToEdgeOpts)

type Config = Array (Int, Int) (Maybe Edge4)

blankConfig :: Config
blankConfig = listArray ((0, 0), (side - 1, side - 1)) $ repeat Nothing

(!?) :: Ix i => Array i e -> i -> Maybe e
a !? i = if inRange (bounds a) i then Just $ a ! i else Nothing

getNeighbours :: (Int, Int) -> Config
                 -> (Maybe Edge, Maybe Edge, Maybe Edge, Maybe Edge)
getNeighbours (i, j) arr = let ml = fmap rightEdge  . join $ arr !? (i + 1, j)
                               mt = fmap bottomEdge . join $ arr !? (i, j - 1)
                               mr = fmap leftEdge   . join $ arr !? (i - 1, j)
                               mb = fmap topEdge    . join $ arr !? (i, j + 1)
                           in (ml, mt, mr, mb)

fitsAt :: Edge4 -> (Int, Int) -> Config -> Bool
fitsAt (l, t, r, b) ij arr = let (ml, mt, mr, mb) = getNeighbours ij arr
                             in and . catMaybes $ [ (l ==) <$> ml
                                                  , (t ==) <$> mt
                                                  , (r ==) <$> mr
                                                  , (b ==) <$> mb ]

findComplete :: ParsedInput -> Array (Int, Int) (Int, Transformation)
findComplete i
        = let done  = null . fst . snd
              mp = edge4IDMap i
              nexts (cfg, (unmatched, m))
                = let ij  = head unmatched
                      ess = M.toList
                            $ M.filterWithKey (\es _ -> (es `fitsAt` ij) cfg) m
                      f (e4, (d, _))
                          = ( cfg // [(ij, Just e4)]
                            , ( tail unmatched, M.filter ((/= d) . fst) m ) )
                  in fmap f ess
              result = fst . fromJust
                       $ dfs done nexts ( blankConfig
                                        , (range ((0, 0), (side - 1, side - 1))
                                        , mp) )
          in array (bounds result) $ fmap ((mp M.!) . fromJust) <$> assocs result

star1 :: ParsedInput -> Int
star1 i = let a = findComplete i
          in product . fmap fst $ curry (a !)
             <$> [0, side - 1] <*> [0, side - 1]

trim :: [String] -> [String]
trim = transpose . tail . init . transpose . tail . init

recoverImage :: ParsedInput -> Array (Int, Int) Char
recoverImage inp
        = let arr = findComplete inp
              m = M.fromList inp
              k = 8 * side - 1
              f i j = let (a, x) = i `divMod` 8
                          (b, y) = j `divMod` 8
                          (d, t) = arr ! (a, b)
                      in (!! x) . (!! y) . trim $ blockApply t (m M.! d)
              assoclist = [ ((i, j), f i j) | i <- [0..k], j <- [0..k] ]
          in array ((0, 0), (k, k)) assoclist

arrayToSet :: Array (Int, Int) Char -> S.Set (Int, Int)
arrayToSet arr = S.fromList [ p | p <- indices arr
                                    , arr ! p == '#' ]

setApply :: Transformation -> S.Set (Int, Int) -> S.Set (Int, Int)
setApply t = let k = 8 * side - 1
             in case t of
                  Rotate 1 -> S.map (\(i, j) -> (k - j, i))
                  Rotate n -> (!! n) . iterate (setApply (Rotate 1))
                  (TransposeRotate 0)
                           -> S.map (\(i, j) -> (j, i))
                  (TransposeRotate n)
                           -> setApply (Rotate n) . setApply (TransposeRotate 0)

seaMonster :: S.Set (Int, Int)
seaMonster = let m = [ "                  # "
                     , "#    ##    ##    ###"
                     , " #  #  #  #  #  #   " ]
             in S.fromList [ (i, j) | i <- [0..19], j <- [0..2]
                                    , (m !! j) !! i == '#' ]

seaMonsters :: [S.Set (Int, Int)]
seaMonsters = (\p -> S.map (translate p) seaMonster)
               <$> [ (i, j) | i <- [0..8 * side - 20], j <- [0..8 * side - 3] ]

translate :: (Int, Int) -> (Int, Int) -> (Int, Int)
translate (a, b) (x, y) = (a + x, b + y)

star2 :: ParsedInput -> Int
star2 inp = let s0 = arrayToSet $ recoverImage inp
                ss = (`setApply` s0) <$> allTfs
                s' = snd . fromJust . find (uncurry S.isSubsetOf)
                     $ (,) <$> seaMonsters <*> ss
                containedmons = filter (`S.isSubsetOf` s') seaMonsters
            in S.size . S.difference s'
               $ foldl' S.union S.empty containedmons

solve :: Solver ParsedInput Int Int
solve i = (star1 i, star2 i)
