module Advent.Y2020.Day07 where

import Advent.Utils
import Data.Tuple (swap)
import qualified Data.Map as M
import qualified Data.Set as S
import Data.Char

type ParsedInput s = [(s, [(Int, s)])]

parseInput :: InputStream s => s -> ParsedInput s
parseInput s = case parse (many line) "" s of
                  Right is -> is
                  Left _   -> error "Parse failed."

bag :: InputStream s => Parser s s
bag = do b <- takeWhile1P Nothing isLetter
                <> string " "
                <> takeWhile1P Nothing isLetter
         hspace
         _ <- try (string "bags") <|> string "bag"
         _ <- try (void $ satisfy (`elem` (".," :: String))) <|> pure ()
         hspace
         pure b

numberedBag :: InputStream s => Parser s (Int, s)
numberedBag = do n <- decimal
                 hspace
                 b <- bag
                 pure (n, b)

line :: InputStream s => Parser s (s, [(Int, s)])
line = do b <- bag
          _ <- string "contain "
          rs <- try (some numberedBag) <|> (string "no other bags." $> [])
          _ <- eol
          pure (b, rs)

parentMap :: InputStream s => [(s, [(Int, s)])] -> M.Map s (S.Set s)
parentMap = foldl (flip (uncurry (M.insertWith S.union))) M.empty
            . fmap (fmap S.singleton . swap)
            . concatMap (traverse (fmap snd))

star1 :: InputStream s => [(s, [(Int, s)])] -> Int
star1 i = S.size $ go S.empty "shiny gold"
                where go s b = case M.lookup b (parentMap i) of
                                 Nothing -> s
                                 Just ps -> let ns = ps `S.difference` s
                                                s' = s  `S.union`      ns
                                            in if S.null ns
                                                  then s
                                                  else S.foldl' go s' ns

star2 :: InputStream s => [(s, [(Int, s)])] -> Int
star2 i = let m     = M.fromList i
              go st = case m M.! st of
                          [] -> 0
                          cs -> sum (fmap fst cs)
                                + sum (fmap (uncurry (*) . fmap go) cs)
           in go "shiny gold"

solve :: InputStream s => Solver (ParsedInput s) Int Int
solve i = (star1 i, star2 i)
