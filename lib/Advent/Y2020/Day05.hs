module Advent.Y2020.Day05 where

import Advent.Utils
import Data.Bits
import Data.Word

type ParsedInput = [Word16]

parseInput :: InputStream s => s -> ParsedInput
parseInput s = case parse (many line) "" s of
                  Right is -> is
                  Left _   -> error "Parse failed."

line :: InputStream s => Parser s Word16
line = go (0 :: Word16)
        where go n = do c <- anySingle
                        if c `elem` ("BR" :: String)
                          then go (shift n 1 + 1)
                          else if c == '\n' then pure n
                                            else go (shift n 1)

solve :: Solver ParsedInput Word16 Word16
solve (k : ks) = let (mx, mn, s)
                         = foldl' (\(a, b, c) d -> (max a d, min b d, c + d))
                                  (k, k, k) ks
                     miss = ( ((mx - mn + 1) * (mx + mn)) `div` 2) - s
                 in (mx, miss)
solve _        = error "Unexpected input."
