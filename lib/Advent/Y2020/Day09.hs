module Advent.Y2020.Day09 where

import Advent.Utils
import Advent.Y2020.Day01 (find2)
import qualified Data.Vector as V

type ParsedInput = [Int]

parseInput :: InputStream s => s -> ParsedInput
parseInput s = case parse (many ((decimal :: InputStream s => Parser s Int)
                                  <* eol)) "" s of
                  Right is -> is
                  Left _   -> error "Parse failed."

star1 :: [Int] -> Int
star1 xs@(_ : ns) = case splitAt 25 xs of
                       (ys, (t : _)) -> if null $ find2 t ys
                                            then t
                                            else star1 ns
                       _ -> error "Unexpected input: empty."
star1 _ = error "Unexpected input: empty."

starBoth :: [Int] -> (Int, Int)
starBoth ns
    = let tgt    = star1 ns
          vec = V.fromList ns
          goUp :: Int -> Int -> Int -> Int
          goUp !s i j | s == tgt
             = let xs = V.slice i (j - i + 1) vec in V.maximum xs + V.minimum xs
          goUp !s i j
                 = case compare s tgt of
                     LT -> let j'     = j + 1
                               r      = vec V.! j'
                           in  goUp (s + r) i j'
                     GT -> goDown (s - (vec V.! i)) (i + 1) j
                     _  -> error "starBoth: Should not happen."
          goDown :: Int -> Int -> Int -> Int
          goDown !s i j | s == tgt
             = let xs = V.slice i (j - i + 1) vec in V.maximum xs + V.minimum xs
          goDown !s i j
                 = case compare s tgt of
                     GT -> goDown (s - (vec V.! j)) i (j - 1)
                     LT -> let j'     = j + 1
                               r      = vec V.! j'
                           in  goUp (s + r) i j'
                     _  -> error "starBoth: Should not happen."
      in (tgt, goUp (vec V.! 0) 0 0)

solve :: Solver ParsedInput Int Int
solve = starBoth
