module Advent.Y2020.Day24 where

import Advent.Utils
import qualified Data.Map.Strict as M
import qualified Data.Set as S

data HexDir = E | NE | NW | W | SW | SE deriving (Eq, Ord, Show)

type ParsedInput = [[HexDir]]

type Hex = (Int, Int)

dirToHex :: HexDir -> Hex
dirToHex E  = ( 1,  0)
dirToHex NE = ( 0,  1)
dirToHex NW = (-1,  1)
dirToHex W  = (-1,  0)
dirToHex SW = ( 0, -1)
dirToHex SE = ( 1, -1)

addH :: Hex -> Hex -> Hex
addH (x1, y1) (x2, y2) = (x1 + x2, y1 + y2)

sumH :: [Hex] -> Hex
sumH = foldl' addH (0, 0)

parseInput :: InputStream s => s -> ParsedInput
parseInput s = case parse (many line) "" s of
                Right i -> i
                _       -> error "Parse failed."

dir :: InputStream s => Parser s HexDir
dir = (single 'n' *> (single 'e' $> NE <|> single 'w' $> NW))
      <|> (single 's' *> (single 'e' $> SE <|> single 'w' $> SW))
      <|> single 'e' $> E
      <|> single 'w' $> W

line :: InputStream s => Parser s [HexDir]
line = some dir <* eol

initBlacks :: ParsedInput -> S.Set Hex
initBlacks = M.keysSet . M.filter (odd :: Int -> Bool) . M.fromListWith (+)
             . fmap ((, 1) . sumH . fmap dirToHex)

star1 :: ParsedInput -> Int
star1 = S.size . initBlacks

neighbours :: Hex -> [Hex]
neighbours h = (h :) $ addH h . dirToHex <$> [E, NE, NW, W, SW, SE]

step :: S.Set Hex -> S.Set Hex
step s = let xs = S.toList s
             m  = M.fromListWith (+)
                  $ concatMap (fmap (, 1 :: Int) . neighbours) xs
             f k v = if k `S.member` s
                       then v == 2 || v == 3
                       else v == 2
         in M.keysSet $ M.filterWithKey f m

star2 :: ParsedInput -> Int
star2 = S.size . (!! 100) . iterate step . initBlacks

solve :: Solver ParsedInput Int Int
solve i = (star1 i, star2 i)
