module Advent.Y2020.Day22 where

import Advent.Utils
import Data.Char
import qualified Data.Vector as V
import qualified Data.Set as S
import Control.Monad.State

type ParsedInput = (V.Vector Int, V.Vector Int)

parseInput :: InputStream s => s -> ParsedInput
parseInput s = case parse input "" s of
                Right i -> i
                _       -> error "Parse failed."

input :: InputStream s => Parser s ParsedInput
input = do takeWhile1P' isPrint >> eol $> ()
           xs <- V.fromList <$> many (decimal <* eol)
           eol $> ()
           takeWhile1P' isPrint >> eol $> ()
           (xs, ) . V.fromList <$> some (decimal <* eol)

star1 :: ParsedInput -> Int
star1 = go
      where go (xs, V.null -> True) = sum $ V.zipWith (*) (V.fromList [1..])
                                            $ V.reverse xs
            go (V.splitAt 1 -> (x, xs), V.splitAt 1 -> (y, ys))
                                = if V.head x > V.head y
                                     then go ( xs V.++ x V.++ y, ys)
                                     else go ( ys V.++ y V.++ x, xs)

scoreHand :: V.Vector Int -> Int
scoreHand = sum . V.zipWith (*) (V.fromList [1..]) . V.reverse

data GameState = GameState { p1hands    :: S.Set (V.Vector Int)
                           , p2hands    :: S.Set (V.Vector Int)
                           , stack      :: [ ( S.Set (V.Vector Int)
                                             , S.Set (V.Vector Int) ) ]
                           }

initState :: GameState
initState = GameState S.empty S.empty []

push :: State GameState ()
push = do gs@GameState { p1hands = p1hs, p2hands = p2hs, stack = st } <- get
          put $ gs { p1hands = S.empty
                   , p2hands = S.empty
                   , stack = (p1hs, p2hs) : st }

pop :: State GameState ()
pop = do gs@GameState { stack = st } <- get
         case st of
          ((s, s') : ss) -> put $ gs { p1hands = s
                                     , p2hands = s'
                                     , stack   = ss }
          _              -> put $ gs { p1hands = S.empty
                                     , p2hands = S.empty
                                     , stack   = [] }

concludeGame :: (V.Vector Int, Int) -> State GameState (V.Vector Int, Int)
concludeGame r = pop >> pure r

playHand :: (V.Vector Int, V.Vector Int) -> State GameState (V.Vector Int, Int)
playHand (V.null -> True, ys) = concludeGame (ys, 2)
playHand (xs, V.null -> True) = concludeGame (xs, 1)
playHand hands = do
    gs@GameState { p1hands = p1hs, p2hands = p2hs } <- get
    case hands of
        (a@(V.splitAt 1 -> (x, xs)), b@(V.splitAt 1 -> (y, ys)))
            | a `S.member` p1hs || b `S.member` p2hs
                -> concludeGame (a, 1)
            | V.head x <= V.length xs && V.head y <= V.length ys
                -> do put $ gs { p1hands = fst hands `S.insert` p1hs
                               , p2hands = snd hands `S.insert` p2hs }
                      push
                      (_, p) <- playHand ( V.take (V.head x) xs
                                         , V.take (V.head y) ys )
                      case p of
                        1 -> playHand (xs V.++ x V.++ y, ys)
                        2 -> playHand (xs, ys V.++ y V.++ x)
                        _ -> error "Impossible."
            | V.head x > V.head y
                -> do put $ gs { p1hands = fst hands `S.insert` p1hs
                               , p2hands = snd hands `S.insert` p2hs }
                      playHand (xs V.++ x V.++ y, ys)
            | V.head x < V.head y
                -> do put $ gs { p1hands = fst hands `S.insert` p1hs
                               , p2hands = snd hands `S.insert` p2hs }
                      playHand (xs, ys V.++ y V.++ x)
            | otherwise -> error "Should not happen."

star2 :: ParsedInput -> Int
star2 i = scoreHand . fst $ evalState (playHand i) initState

solve :: Solver ParsedInput Int Int
solve i = (star1 i, star2 i)
