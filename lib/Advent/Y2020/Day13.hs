module Advent.Y2020.Day13 where

import Advent.Utils
import Data.Maybe
import Data.Foldable (foldlM)

type ParsedInput = (Integer, [Maybe Integer])

parseInput :: InputStream s => s -> ParsedInput
parseInput s = case parse ((,) <$> time <*> buses) "" s of
                 Right r -> r
                 Left _  -> error "Parse failed."

time :: InputStream s => Parser s Integer
time = decimal <* eol

bus :: InputStream s => Parser s (Maybe Integer)
bus = try (Just <$> decimal) <|> char 'x' $> Nothing

buses :: InputStream s => Parser s [Maybe Integer]
buses = (bus `sepBy1` char ',') <* eol

star1 :: ParsedInput -> Integer
star1 (t, ms) = (\(t', n) -> (t' - t) * n) . minimum . fmap f . catMaybes $ ms
        where f n = (t + n - (t `mod` n), n)

processSchedule :: [Maybe Integer] -> [(Integer, Integer)]
processSchedule = go 0
          where go k (Just n  : xs) = (- k, n) : go (k + 1) xs
                go k (Nothing : xs) =            go (k + 1) xs
                go _ []             =            []

-- |euclid a b gives ((x, y), d) such that d = gcd a b = xa + yb.
euclid :: (Ord a, Num a, Integral a) => a -> a -> ((a, a), a)
euclid a b | a < 0 && b < 0 = euclid (-a) b
euclid a b | a < 0 = (\((x, y), d) -> ((-x, y), d)) $ euclid (-a) b
euclid a b | a < b = (\((x, y), d) -> ((y, x), d)) $ euclid b a
euclid a b = go a b (1, 0) (0, 1)
        where go x y (c1, d1) (c2, d2)
                = case x `divMod` y of
                     (_, 0)  -> ((c2, d2), c2 * a + d2 * b)
                     (q, r') -> go y r' (c2, d2) (c1 - q * c2, d1 - q * d2)

crt :: (Integer, Integer) -> (Integer, Integer) -> Maybe (Integer, Integer)
crt (a1, n1) (a2, n2) = let ((x1, x2), d) = euclid n1 n2
                            c1       = x2 * n2
                            c2       = x1 * n1
                            n        = n1 * n2 `div` d
                        in if (a1 - a2) `mod` d == 0
                           then Just (div (a1 * c1 + a2 * c2) d `mod` n, n)
                           else Nothing

star2 :: ParsedInput -> Integer
star2 i = case foldlM crt (0, 1) . processSchedule . snd $ i of
            Just (r, _) -> r
            Nothing     -> error "Encountered non-coprime moduli."

solve :: Solver ParsedInput Integer Integer
solve i = (star1 i, star2 i)
