module Advent.Y2020.Day25 where

import Advent.Utils
import Advent.Y2020.Day13 (euclid)
import Data.Maybe
import qualified Data.HashMap.Lazy as HM

type ParsedInput = (Int, Int)

parseInput :: InputStream s => s -> ParsedInput
parseInput s = case parse input "" s of
                Right r -> r
                _       -> error "Parse failed."

indexMod :: Int -- ^ base, must be a primitive root
            -> Int -- ^ target
            -> Int -- ^ modulus
            -> Int -- ^ result
indexMod b t q = let m = ceiling . (sqrt :: Float -> Float) . fromIntegral $ q
                     hm = HM.fromList [ (kToTheNModM b j q, j) | j <- [0..m] ]
                     (((`mod` q) -> x, _), _) = euclid (kToTheNModM b m q) q
                     go i g = case HM.lookup g hm of
                                Nothing -> go (i + 1) $ (g * x) `mod` q
                                Just j  -> i * m + j
                 in go 0 t

indexMods :: Int      -- ^ base, must be a primitive root
            -> [Int] -- ^ targets
            -> Int   -- ^ modulus
            -> [Int] -- ^ result
indexMods b ts q = let m = ceiling . (sqrt :: Float -> Float) . fromIntegral $ q
                       hm = HM.fromList [ (kToTheNModM b j q, j) | j <- [0..m] ]
                       (((`mod` q) -> x, _), _) = euclid (kToTheNModM b m q) q
                       go i rs gs = let rs' = zipWith (<|>) rs
                                                $ fmap (\j -> i * m + j)
                                                  . (`HM.lookup` hm) <$> gs
                                    in if all isJust rs'
                                         then catMaybes rs'
                                         else go (i + 1) rs'
                                              $ (\g -> (g * x) `mod` q) <$> gs
                   in go 0 (const Nothing <$> ts) ts

input :: InputStream s => Parser s (Int, Int)
input = do n1 <- decimal <* eol
           (n1, ) <$> decimal <* eol

kToTheNModM :: Int -> Int -> Int -> Int
kToTheNModM _ 0 _ = 1
kToTheNModM k 1 m = k `mod` m
kToTheNModM k n m | even n
                    = let n' = n `div` 2
                      in (kToTheNModM k n' m ^ (2 :: Int)) `mod` m
kToTheNModM k n m | odd n
                    = let n' = (n - 1) `div` 2
                      in k * ((kToTheNModM k n' m ^ (2 :: Int)) `mod` m) `mod` m
kToTheNModM _ _ _ = error "Impossible."

star1 :: ParsedInput -> Int
star1 (n1, n2) = let m  = 20201227
                     -- [e1, e2] = indexMods 7 [n1, n2] m
                     e1 = indexMod 7 n1 m
                     e2 = indexMod 7 n2 m
                 in kToTheNModM 7 (e1 * e2) m

star2 :: ParsedInput -> String
star2 = const "AoC 2020 Complete!"

solve :: Solver ParsedInput Int String
solve i = (star1 i, star2 i)
