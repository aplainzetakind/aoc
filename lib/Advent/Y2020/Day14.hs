module Advent.Y2020.Day14 where

import Advent.Utils
import Data.Word
import Data.Bits
import qualified Data.Map.Strict as M

type ParsedInput = [([(Word64, Word64)], MaskString)]

type MaskString = String

data Mask = Mask { toAnd :: Word64
                  , toOr  :: Word64 } deriving Show

identityMask :: Mask
identityMask = Mask { toAnd = 2 ^ (36 :: Word64) - 1
                      , toOr  = minBound }

parseInput :: InputStream s => s -> ParsedInput
parseInput s = case parse (many maskGroup) "" s of
                  Right is -> is
                  Left _   -> error "Parse failed."

maskGroup :: InputStream s => Parser s ([(Word64, Word64)], MaskString)
maskGroup = do _    <- string "mask = "
               ms   <- maskString
               mems <- many memInst
               pure (mems, ms)

memInst :: InputStream s => Parser s (Word64, Word64)
memInst = do _ <- string "mem["
             m <- decimal
             _ <- string "] = "
             v <- decimal <* eol
             pure (m, v)

maskString :: InputStream s => Parser s String
maskString = chunkToTokens' <$> takeWhileP' (`elem` ("01X" :: String)) <* eol

makeMask :: String -> Mask
makeMask s = case parse mask "" s of
                Right st -> st
                _        -> error $ "Can't parse mask 1: " ++ s

mask :: InputStream s => Parser s Mask
mask = go 35 identityMask
        where go (- 1) m = pure m
              go n m@(Mask a o )
                   = do c <- anySingle
                        case c of
                          'X' -> go (n - 1) m
                          '1' -> go (n - 1) $ Mask a (o `setBit` n)
                          '0' -> go (n - 1) $ Mask (a `clearBit` n) o
                          _   -> error $ "Unexpected character "
                                          ++ "in mask: " ++ show c

applyMask :: Mask -> Word64 -> Word64
applyMask (Mask a o) = (.&. a) . (.|. o)

applyMaskGroup :: ([(Word64, Word64)], MaskString) -> [(Word64, Word64)]
applyMaskGroup (mems, m) = fmap (applyMask (makeMask m)) <$> mems

writeAndSum :: [(Word64, Word64)] -> Word64
writeAndSum = sum . M.elems . foldl' (\m (k, v) -> M.insert k v m) M.empty

star1 :: ParsedInput -> Word64
star1 = writeAndSum . concatMap applyMaskGroup

applyMaskGroupMem :: ([(Word64, Word64)], MaskString) -> [(Word64, Word64)]
applyMaskGroupMem (mems, m) = (\(a, b) -> (applyMask (makeMask m) a, b))
                              <$> mems

maskString2ToMaskString1s :: String -> [MaskString]
maskString2ToMaskString1s = foldr f [[]]
          where f c xss = case c of
                            '0' -> ('X' :) <$> xss
                            '1' -> ('1' :) <$> xss
                            'X' -> (\xs -> ['1' : xs, '0' : xs]) =<< xss
                            _   -> error "Invalid mask."

star2 :: ParsedInput -> Word64
star2 = writeAndSum
        . concatMap applyMaskGroupMem
        . concatMap (traverse maskString2ToMaskString1s)

solve :: Solver ParsedInput Word64 Word64
solve i = (star1 i, star2 i)
