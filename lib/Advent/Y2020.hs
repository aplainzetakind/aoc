{-# LANGUAGE RankNTypes #-}
module Advent.Y2020 where

import Advent.Utils
import qualified Advent.Y2020.Day01 as Day01
import qualified Advent.Y2020.Day02 as Day02
import qualified Advent.Y2020.Day03 as Day03
import qualified Advent.Y2020.Day04 as Day04
import qualified Advent.Y2020.Day05 as Day05
import qualified Advent.Y2020.Day06 as Day06
import qualified Advent.Y2020.Day07 as Day07
import qualified Advent.Y2020.Day08 as Day08
import qualified Advent.Y2020.Day09 as Day09
import qualified Advent.Y2020.Day10 as Day10
import qualified Advent.Y2020.Day11 as Day11
import qualified Advent.Y2020.Day12 as Day12
import qualified Advent.Y2020.Day13 as Day13
import qualified Advent.Y2020.Day14 as Day14
import qualified Advent.Y2020.Day15 as Day15
import qualified Advent.Y2020.Day16 as Day16
import qualified Advent.Y2020.Day17 as Day17
import qualified Advent.Y2020.Day18 as Day18
import qualified Advent.Y2020.Day19 as Day19
import qualified Advent.Y2020.Day20 as Day20
import qualified Advent.Y2020.Day21 as Day21
import qualified Advent.Y2020.Day22 as Day22
import qualified Advent.Y2020.Day23 as Day23
import qualified Advent.Y2020.Day24 as Day24
import qualified Advent.Y2020.Day25 as Day25

mkSol :: InputStream s => (Show b, Show c) => (s -> a) -- ^ Parser.
                          -> Solver a b c
                          -> Solver s String String
mkSol p f = (\(b, c) -> (show b, show c)) . f . p

advent :: InputStream s => Int -> Solver s String String
advent = \case
            1  -> mkSol Day01.parseInput Day01.solve
            2  -> mkSol Day02.parseInput Day02.solve
            3  -> mkSol Day03.parseInput Day03.solve
            4  -> mkSol Day04.parseInput Day04.solve
            5  -> mkSol Day05.parseInput Day05.solve
            6  -> mkSol Day06.parseInput Day06.solve
            7  -> mkSol Day07.parseInput Day07.solve
            8  -> mkSol Day08.parseInput Day08.solve
            9  -> mkSol Day09.parseInput Day09.solve
            10 -> mkSol Day10.parseInput Day10.solve
            11 -> mkSol Day11.parseInput Day11.solve
            12 -> mkSol Day12.parseInput Day12.solve
            13 -> mkSol Day13.parseInput Day13.solve
            14 -> mkSol Day14.parseInput Day14.solve
            15 -> mkSol Day15.parseInput Day15.solve
            16 -> mkSol Day16.parseInput Day16.solve
            17 -> mkSol Day17.parseInput Day17.solve
            18 -> mkSol Day18.parseInput Day18.solve
            19 -> mkSol Day19.parseInput Day19.solve
            20 -> mkSol Day20.parseInput Day20.solve
            21 -> mkSol Day21.parseInput Day21.solve
            22 -> mkSol Day22.parseInput Day22.solve
            23 -> mkSol Day23.parseInput Day23.solve
            24 -> mkSol Day24.parseInput Day24.solve
            25 -> mkSol Day25.parseInput Day25.solve
            _  -> error "Invalid day number."
