module Advent.Y2015.Day25 where

import Advent.Utils
import Data.Char

parseInput :: Input -> (Word, Word)
parseInput = (\[a, b] -> (a, b))
             . fmap read
             . filter (all isDigit)
             . words
             . filter (`notElem` ".,")

order :: (Word, Word) -> Word
order (m, n) = let k = m + n - 2
               in (+ n) $ k * (k + 1) `div` 2

kToTheNModM :: Word -> Word -> Word -> Word
kToTheNModM k 0 m = 1
kToTheNModM k 1 m = k `mod` m
kToTheNModM k n m | even n
                    = let n' = n `div` 2
                      in (kToTheNModM k n' m ^ 2) `mod` m
kToTheNModM k n m | odd  n
                    = let n' = (n - 1) `div` 2
                      in k * ((kToTheNModM k n' m ^ 2) `mod` m) `mod` m

star1 :: (Word, Word) -> Word
star1 = (\n -> (20151125 * kToTheNModM 252533 (n - 1) 33554393) `mod` 33554393)
        . order

star2 :: Input
star2 = "AoC2015 complete!"

solution :: Stars -> Input -> Result
solution s i = case s of
                  First  -> pure . show . star1 . parseInput $ i
                  Second -> pure $ star2
                  Both   -> (`solution` i) =<< [First, Second]
