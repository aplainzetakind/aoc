module Advent.Y2015.Day23 where

import Advent.Utils
import Control.Monad.State
import Data.Array
import Control.Arrow
import Control.Monad.Extra (ifM)

type Computer a = State MState a

data MState = MState { regA :: Int
                     , regB :: Int
                     , ip   :: Int
                     , prog :: Array Int Instr
                     } deriving Show

data Reg = RA | RB deriving Show

data Instr = HLF Reg
           | TPL Reg
           | INC Reg
           | JMP Int
           | JIE Reg Int
           | JIO Reg Int deriving Show

readReg :: Input -> Reg
readReg "a" = RA
readReg "b" = RB

readInst :: Input -> Instr
readInst s = case words . filter (`notElem` ",+") $ s of
                ["hlf", r]    -> HLF $ readReg r
                ["tpl", r]    -> TPL $ readReg r
                ["inc", r]    -> INC $ readReg r
                ["jmp", n]    -> JMP $ read n
                ["jie", r, n] -> JIE (readReg r) (read n)
                ["jio", r, n] -> JIO (readReg r) (read n)

parseInput :: Input -> Array Int Instr
parseInput s = let s' = readInst <$> lines s
                   n  = length s'
               in listArray (0, n - 1) s'

run :: Computer Int
run = do s@MState{ ip = p, prog = m } <- get
         let bs = bounds m
         if uncurry (&&) $ (p >=) *** (p <=) $ bs
           then exec $ m ! p
           else pure $ regB s

exec :: Instr -> Computer Int
exec (HLF RA) = do s@MState { regA = r, ip = p } <- get
                   put s { regA = r `div` 2, ip = p + 1 }
                   run
exec (HLF RB) = do s@MState { regB = r, ip = p } <- get
                   put s { regB = r `div` 2, ip = p + 1 }
                   run
exec (TPL RA) = do s@MState { regA = r, ip = p } <- get
                   put s { regA = r * 3, ip = p + 1 }
                   run
exec (TPL RB) = do s@MState { regB = r, ip = p } <- get
                   put s { regB = r * 3, ip = p + 1 }
                   run
exec (INC RA) = do s@MState { regA = r, ip = p } <- get
                   put s { regA = r + 1, ip = p + 1 }
                   run
exec (INC RB) = do s@MState { regB = r, ip = p } <- get
                   put s { regB = r + 1, ip = p + 1 }
                   run
exec (JMP os) = do s@MState { ip = p } <- get
                   put s { ip = p + os }
                   run
exec (JIE RA os) = do s@MState { regA = r, ip = p } <- get
                      ifM (pure $ even r)
                          (put s { ip = p + os })
                          (put s { ip = p + 1 })
                      run
exec (JIE RB os) = do s@MState { regB = r, ip = p } <- get
                      ifM (pure $ even r)
                          (put s { ip = p + os })
                          (put s { ip = p + 1 })
                      run
exec (JIO RA os) = do s@MState { regA = r, ip = p } <- get
                      ifM (pure $ r == 1)
                          (put s { ip = p + os })
                          (put s { ip = p + 1 })
                      run
exec (JIO RB os) = do s@MState { regB = r, ip = p } <- get
                      ifM (pure $ r == 1)
                          (put s { ip = p + os })
                          (put s { ip = p + 1 })
                      run

star1 :: Array Int Instr -> Int
star1 = evalState run . MState 0 0 0

star2 :: Array Int Instr -> Int
star2 = evalState run . MState 1 0 0

solution :: Stars -> Input -> Result
solution s i = case s of
                  First  -> pure . show . star1 . parseInput $ i
                  Second -> pure . show . star2 . parseInput $ i
                  Both   -> (`solution` i) =<< [First, Second]
