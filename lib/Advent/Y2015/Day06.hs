{-# LANGUAGE FlexibleContexts #-}
module Advent.Y2015.Day06 where

import Advent.Utils
import Data.List.Split (splitOn)
import qualified Data.Map as M
import Data.Array.ST
import Control.Monad.ST
import Data.Foldable

type Input = String

data LampState = On | Off deriving Eq

data Instruction = Toggle (Point, Point) | Turn LampState (Point, Point)

type Grid s a = STUArray s (Int, Int) a

parsePoint :: Input -> (Int, Int)
parsePoint s = let [a, b] = read <$> splitOn "," s
               in (a, b)

parseInstruction :: Input -> Instruction
parseInstruction s
      = case words s of
          ["turn","on",p1,_,p2]  -> Turn On  ( parsePoint p1, parsePoint p2 )
          ["turn","off",p1,_,p2] -> Turn Off ( parsePoint p1, parsePoint p2 )
          ["toggle",p1,_,p2]     -> Toggle   ( parsePoint p1, parsePoint p2 )

parseInput :: Input -> [Instruction]
parseInput = fmap parseInstruction . lines

initGrid :: ST s (Grid s Bool)
initGrid = newArray ((0, 0), (999, 999)) False

initGrid2 :: ST s (Grid s Int)
initGrid2 = newArray ((0, 0), (999, 999)) 0

applyAtPoint :: (MArray a e m, Ix i) => a i e -> (e -> e) -> i -> m ()
applyAtPoint a f i = writeArray a i . f =<< readArray a i

runInstruction :: MArray a Bool m => a Point Bool -> Instruction -> m ()
runInstruction a (Toggle pp)
          = traverse_ (applyAtPoint a not) $ range pp
runInstruction a (Turn On pp)
          = traverse_ (applyAtPoint a (const True)) $ range pp
runInstruction a (Turn Off pp)
          = traverse_ (applyAtPoint a (const False)) $ range pp

runInstruction2 :: MArray a Int m => a Point Int -> Instruction -> m ()
runInstruction2 a (Toggle pp)
          = traverse_ (applyAtPoint a (+ 2)) $ range pp
runInstruction2 a (Turn On pp)
          = traverse_ (applyAtPoint a (+ 1)) $ range pp
runInstruction2 a (Turn Off pp)
          = traverse_ (applyAtPoint a (\x -> min (x - 1) 0)) $ range pp

star1 :: [Instruction] -> Int
star1 is = runST $ do a <- initGrid
                      traverse_ (runInstruction a) is
                      length . filter id <$> getElems a

star2 :: [Instruction] -> Int
star2 is = runST $ do a <- initGrid2
                      traverse_ (runInstruction2 a) is
                      sum <$> getElems a

solution :: Stars -> Input -> Result
solution s i = case s of
                  First  -> pure . show . star1 . parseInput $ i
                  Second -> pure . show . star2 . parseInput $ i
                  Both   -> (`solution` i) =<< [First, Second]
