module Advent.Y2015.Day15 where

import Advent.Utils
import Data.Function (on)
import Data.List
import qualified Data.Set as S

data Ingredient = Ingredient { getName       :: Input
                             , getCapacity   :: Int
                             , getDurability :: Int
                             , getFlavor     :: Int
                             , getTexture    :: Int
                             , getCalories   :: Int
                             } deriving Show

parseIngredient :: Input -> Ingredient
parseIngredient s | Just [n, c, d, f, t, cal] <- terms [0,2,4,6,8,10 ]
                                           . fmap (filter (not . (`elem` ",:")))
                                           . words $ s
                    = Ingredient { getName       = n
                                 , getCapacity   = read c
                                 , getDurability = read d
                                 , getFlavor     = read f
                                 , getTexture    = read t
                                 , getCalories   = read cal
                                 }

parseInput :: Input -> [Ingredient]
parseInput = fmap parseIngredient . lines

ingrToList :: Ingredient -> [Int]
ingrToList Ingredient { getName       = a
                      , getCapacity   = b
                      , getDurability = c
                      , getFlavor     = d
                      , getTexture    = e
                      , getCalories   = f
                      } = [b, c, d, e, f]


partitions :: Int -> Int -> [[Int]]
partitions 1 n = [[n]]
partitions k n = concat
                    $ (\i -> (i :) <$> partitions (k - 1) (n - i)) <$> [0..n]

zeroIfNeg :: Int -> Int
zeroIfNeg n = if n < 0 then 0 else n

combine :: [Int] -> [Ingredient] -> [Int]
combine p = fmap (zeroIfNeg . sum) . transpose
            . zipWith (fmap . (*)) p . fmap ingrToList

score = product . init

star1 :: [Ingredient] -> Int
star1 is = let k = length is
               ps = partitions k 100
           in maximum . fmap score $ (`combine` is) <$> ps

star2 :: [Ingredient] -> Int
star2 is = let k = length is
               ps = partitions k 100
           in maximum . fmap score . filter ((== 500) . last)
              $ (`combine` is) <$> ps

solution :: Stars -> Input -> Result
solution s i = case s of
                  First  -> pure . show . star1 . parseInput $ i
                  Second -> pure . show . star2 . parseInput $ i
                  Both   -> (`solution` i) =<< [First, Second]
