module Advent.Y2015.Day07 where

import Advent.Utils
import qualified Data.Map.Lazy as M
import Data.Word
import Data.Bits
import Data.Char (isDigit)
import Debug.Trace

data In = WIRE Input | LIT Word16 deriving (Show, Eq, Ord)

data Connection = RELAY In
                | AND In In
                | OR In In
                | LSHIFT In Int
                | RSHIFT In Int
                | NOT In deriving Show

parseIn :: Input -> In
parseIn s = if all isDigit s then LIT (read s) else WIRE s

parseConn :: Input -> (In, Connection)
parseConn s = case words s of
                 ["NOT", y, "->", w]       -> (WIRE w, NOT $ parseIn y)
                 [x, "->", w]              -> (WIRE w, RELAY $ parseIn x)
                 [x, "AND", y, "->", w]    -> (WIRE w, AND (parseIn x)
                                                      (parseIn y))
                 [x, "OR", y, "->", w]     -> (WIRE w, OR (parseIn x)
                                                          (parseIn y))
                 [v, "LSHIFT", y, "->", w] -> (WIRE w, LSHIFT (parseIn v)
                                                         (read y))
                 [v, "RSHIFT", y, "->", w] -> (WIRE w, RSHIFT (parseIn v)
                                                              (read y))

parseInput :: Input -> M.Map In Connection
parseInput = M.fromList . fmap parseConn . lines

runGates :: M.Map In Connection -> M.Map In Word16
runGates m = m'
        where m' = M.map evaluate m
              lookupIfWire = \case
                                LIT  n -> n
                                w      -> m' M.! w
              evaluate c = case c of
                   RELAY a    -> lookupIfWire a
                   AND a b    -> lookupIfWire a .&. lookupIfWire b
                   OR a b     -> lookupIfWire a .|. lookupIfWire b
                   LSHIFT a b -> shift (lookupIfWire a) b
                   RSHIFT a b -> shift (lookupIfWire a) (-b)
                   NOT a      -> complement (lookupIfWire a)

getSignal :: In -> M.Map In Connection -> Word16
getSignal w cm = runGates cm M.! w

star1 :: Input -> Word16
star1 = getSignal (WIRE "a") . parseInput

star2 :: Input -> Word16
star2 s = let cm  = parseInput s
              r   = getSignal (WIRE "a") cm
              cm' = M.insertWith const (WIRE "b") (RELAY (LIT r)) cm
          in getSignal (WIRE "a") cm'

solution :: Stars -> Input -> Result
solution s i = case s of
                  First  -> pure . show . star1 $ i
                  Second -> pure . show . star2 $ i
                  Both   -> let cm  = parseInput i
                                r1  = getSignal (WIRE "a") cm
                                cm' = M.insertWith const (WIRE "b")
                                                 (RELAY (LIT r1)) cm
                                r2  = getSignal (WIRE "a") cm'
                            in fmap show [r1, r2]
