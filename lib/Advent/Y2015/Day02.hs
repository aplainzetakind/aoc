module Advent.Y2015.Day02 where

import Advent.Utils

type Input = String

parseInput :: Input -> [[Int]]
parseInput = fmap makeList . lines
    where makeList :: Input -> [Int]
          makeList = read . ('[' :) . (++ "]") . fmap (\case 'x' -> ','; n -> n)

getPaper :: [Int] -> Int
getPaper is = let rotated = zipWith const ( drop 1 $ cycle is ) is
                  sides   = zipWith (*) rotated is
                  extra   = minimum sides
              in sum ((2*) <$> sides) + extra

getRibbon :: [Int] -> Int
getRibbon is = 2 * (sum is - maximum is) + product is

star1 :: [[Int]] -> Int
star1 = sum . fmap getPaper

star2 :: [[Int]] -> Int
star2 = sum . fmap getRibbon

solution :: Stars -> Input -> Result
solution s i = case s of
                  First  -> pure . show . star1 . parseInput $ i
                  Second -> pure . show . star2 . parseInput $ i
                  Both   -> (`solution` i) =<< [First, Second]
