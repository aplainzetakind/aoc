{-# LANGUAGE RankNTypes #-}
module Advent.Y2015.Day20 where

import Advent.Utils
import Data.Word
import qualified Data.Array.MArray as MA
import Data.Array.ST
import Control.Monad.ST
import Data.Foldable

findPresents :: Word32 -> (forall s. ST s Word32)
findPresents t'
    = let t      = t' `div` 10
          m      = (2 ^) . floor . logBase 2 . fromIntegral $ t
      in do a <- MA.newListArray (2, m) [3..m + 1]
                       :: ST s (STUArray s Word32 Word32)
            let go n = do an <- MA.readArray a n
                          if an >= t then pure n
                            else let f i = do v <- MA.readArray a i
                                              MA.writeArray a i (v + n)
                                 in traverse_ f [2 * n, 3 * n..m]
                                    >> go (n + 1)
            go 2

findPresents' :: Word32 -> (forall s. ST s Word32)
findPresents' t'
    = let t      = t' `div` 11
          m      = (2 ^) . floor . logBase 2 . fromIntegral $ t
      in do a <- MA.newListArray (2, m) [3..m + 1]
                       :: ST s (STUArray s Word32 Word32)
            let go n = do an <- MA.readArray a n
                          if an >= t then pure n
                            else let f i = do v <- MA.readArray a i
                                              MA.writeArray a i (v + n)
                                 in traverse_ f [2 * n, 3 * n..min m (50 * n)]
                                    >> go (n + 1)
            go 2

star1 :: Word32 -> Word32
star1 n = runST $ findPresents n

star2 :: Word32 -> Word32
star2 n = runST $ findPresents' n

solution :: Stars -> Input -> Result
solution s i = case s of
                  First  -> pure . show . star1 . (read :: Input -> Word32) $ i
                  Second -> pure . show . star2 . (read :: Input -> Word32) $ i
                  Both   -> (`solution` i) =<< [First, Second]
