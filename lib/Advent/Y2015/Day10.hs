module Advent.Y2015.Day10 where

import Advent.Utils
import Data.List

lookSay :: Input -> Input
lookSay = concatMap say . group
          where say l@(x : _) = show (length l) ++ [x]

step :: Int -> Input -> Int
step n s = length $ iterate lookSay s !! n

solution :: Stars -> Input -> Result
solution s i = case s of
                  First  -> pure . show . step 40 $ i
                  Second -> pure . show . step 50 $ i
                  Both   -> (`solution` i) =<< [First, Second]
