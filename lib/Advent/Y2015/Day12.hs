module Advent.Y2015.Day12 where

import Advent.Utils
import Data.List
import Data.Char

sumNumbers :: Input -> Int
sumNumbers = go 0
   where go n ('-' : x : xs)
              | isDigit x = let n' = n + read ( '-' : x : takeWhile isDigit xs)
                            in go n' $ dropWhile isDigit xs
         go n ('-' : x : xs)
                          = go n xs
         go n (x : xs)
              | isDigit x = let n' = n + read ( x : takeWhile isDigit xs)
                            in go n' $ dropWhile isDigit xs
         go n (x : xs)
                          = go n xs
         go n _           = n

dropToUnmatched c c' = go 0
    where go 0 (x : xs) | x == c    = xs
          go n (x : xs) | x == c    = go (n - 1) xs
                        | x == c'   = go (n + 1) xs
                        | otherwise = go n xs
          go _ []                   = []

omitRedObjects :: Input -> Input
omitRedObjects = go []
      where go ls (':' : '\"' : 'r' : 'e' : 'd' : '\"' : xs)
                           = go (dropToUnmatched '{' '}' ls)
                                (dropToUnmatched '}' '{' xs)
            go ls (x : xs) = go (x : ls) xs
            go ls _        = reverse ls

star1 :: Input -> Int
star1 = sumNumbers

star2 :: Input -> Int
star2 = sumNumbers . omitRedObjects

solution :: Stars -> Input -> Result
solution s i = case s of
                  First  -> pure . show . star1 $ i
                  Second -> pure . show . star2 $ i
                  Both   -> (`solution` i) =<< [First, Second]
