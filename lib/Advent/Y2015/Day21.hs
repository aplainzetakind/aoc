module Advent.Y2015.Day21 where

import Advent.Utils
import Control.Applicative

data Stats = Stats { hitpoint :: Int
                   , damage   :: Int
                   , armor    :: Int } deriving (Eq, Ord)

type Item = (Int, (Int, Int))

parseInput :: Input -> Stats
parseInput = (\[a, b, c] -> Stats a b c)
             . fmap ((read :: Input -> Int) . last . words) . lines

boss :: Stats
boss = Stats 103 9 2

weapons :: [Item]
weapons = [ ( 8, (4 ,0))
          , (10, (5 ,0))
          , (25, (6 ,0))
          , (40, (7 ,0))
          , (74, (8 ,0)) ]

armors :: [Item]
armors = [ ( 13, (0, 1))
         , ( 31, (0, 2))
         , ( 53, (0, 3))
         , ( 75, (0, 4))
         , (102, (0, 5)) ]

rings :: [Item]
rings = [ ( 25, (1, 0))
        , ( 50, (2, 0))
        , (100, (3, 0))
        , ( 20, (0, 1))
        , ( 40, (0, 2))
        , ( 80, (0, 3)) ]

choices :: [[Item]]
choices = let ws = weapons `choose` 1
              as = armors `choose` 0 ++ armors `choose` 1
              rs = rings `choose` 0 ++ rings `choose` 1 ++ rings `choose` 2
          in concat <$> foldr (liftA2 (:)) [[]] [ws, as, rs]

beats :: Stats -> Stats -> Bool
beats s1@(Stats _ d1 _) s2@(Stats hp2 d2 a2)
     = let d = max 1 $ d1 - a2
           hp2' = hp2 - d
       in hp2' <= 0 || not (beats (Stats hp2' d2 a2) s1)

choiceStats :: [Item] -> (Int, Stats)
choiceStats = fmap (uncurry (Stats 100))
              . foldr (\(a, (b , c)) (x, (y, z)) -> (a + x, (b + y, c + z)))
                      (0, (0, 0))

star1 :: Stats -> Int
star1 s = fst . minimum . filter ((`beats` s) . snd)
          . fmap choiceStats $ choices

star2 :: Stats -> Int
star2 s = fst . maximum . filter (not . (`beats` s) . snd)
          . fmap choiceStats $ choices

solution :: Stars -> Input -> Result
solution s i = case s of
                  First  -> pure . show . star1 . parseInput $ i
                  Second -> pure . show . star2 . parseInput $ i
                  Both   -> (`solution` i) =<< [First, Second]
