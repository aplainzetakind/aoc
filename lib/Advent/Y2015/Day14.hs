module Advent.Y2015.Day14 where

import Advent.Utils
import Data.Ord
import Data.List
import Data.Function
import Data.List.Extra

data Reindeer = Reindeer { getName     :: Input
                         , getSpeed    :: Int
                         , getUptime   :: Int
                         , getDowntime :: Int
                         , getState    :: ReindeerState
                         , getDistance :: Int
                         , getPoints   :: Int } deriving Show

data ReindeerState = Resting Int | Running Int deriving Show

parseLine :: Input -> Reindeer
parseLine s | Just [n, s, u, d] <- terms [0,3,6,13] . words $ s
                   = Reindeer { getName     = n
                              , getSpeed    = read s
                              , getUptime   = read u
                              , getDowntime = read d
                              , getState    = Running 0
                              , getDistance = 0
                              , getPoints   = 0 }
parseLine s = error "parseLine: no parse."

parseInput :: Input -> [Reindeer]
parseInput = fmap parseLine . lines

elapseSecond :: Reindeer -> Reindeer
elapseSecond r@Reindeer { getSpeed    = spd
                        , getUptime   = ut
                        , getDowntime = dt
                        , getState    = st
                        , getDistance = d }
      = case st of
          Resting n -> let n' = n + 1
                       in r { getState = if n' == dt then Running 0
                                                     else Resting n' }
          Running n -> let n' = n + 1
                           d' = d + spd
                       in r { getDistance = d'
                            , getState = if n' == ut then Resting 0
                                                     else Running n' }

increasePoint :: Reindeer -> Reindeer
increasePoint r@Reindeer { getPoints = n } = r { getPoints = n + 1 }

awardLeaders :: [Reindeer] -> [Reindeer]
awardLeaders xs = let (ys : yss) = groupOn getDistance
                                   . sortOn (Down . getDistance) $ xs
                  in fmap increasePoint ys ++ concat yss

elapseAwarding :: [Reindeer] -> [Reindeer]
elapseAwarding = awardLeaders . fmap elapseSecond

star1 :: [Reindeer] -> Int
star1 = maximum . fmap getDistance . (!! 2503)
        . iterate (fmap elapseSecond)

star2 :: [Reindeer] -> Int
star2 = maximum . fmap getPoints . (!! 2503)
        . iterate elapseAwarding

solution :: Stars -> Input -> Result
solution s i = case s of
                  First  -> pure . show . star1 . parseInput $ i
                  Second -> pure . show . star2 . parseInput $ i
                  Both   -> (`solution` i) =<< [First, Second]
