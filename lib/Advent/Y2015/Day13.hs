module Advent.Y2015.Day13 where

import Advent.Utils
import qualified Data.Map as M
import Data.List (nub, permutations)

parseLine :: Input -> (Pair Input, Int)
parseLine s | Just [a, gl, n, b] <- terms [0,2,3,10] . words . init $ s
                   = let f = case gl of
                               "gain" -> id
                               "lose" -> negate
                     in (mkPair a b, f (read n))
parseLine s = error "parseLine: parse error."

parseInput :: Input -> M.Map (Pair Input) Int
parseInput = foldr (uncurry $ M.insertWith (+)) M.empty
             . fmap parseLine .lines

toInputs :: Pair Input -> [Input]
toInputs = (\(x, y) -> [x, y]) . pairToTuple

people :: M.Map (Pair Input) a -> [Input]
people = nub . concatMap toInputs . M.keys

addYou :: M.Map (Pair Input) Int -> M.Map (Pair Input) Int
addYou m = let toAdd = (\x -> (mkPair x "You", 0)) <$> people m
           in foldr (uncurry  M.insert) m toAdd

roundSeat :: Ord a => [a] -> [Pair a]
roundSeat xs = zipWith mkPair xs $ drop 1 xs ++ cycle xs

happinessDiff :: Ord a => M.Map a Int -> [a] -> Int
happinessDiff m = sum . fmap (\x -> m M.! x)

star1 :: M.Map (Pair Input) Int -> Int
star1 m = let ps = people m
              seatings = roundSeat <$> permutations ps
          in maximum $ happinessDiff m <$> seatings

star2 :: M.Map (Pair Input) Int -> Int
star2 m' = let m  = addYou m'
               ps = people m
               seatings = roundSeat <$> permutations ps
           in maximum $ happinessDiff m <$> seatings

solution :: Stars -> Input -> Result
solution s i = case s of
                  First  -> pure . show . star1 . parseInput $ i
                  Second -> pure . show . star2 . parseInput $ i
                  Both   -> (`solution` i) =<< [First, Second]
