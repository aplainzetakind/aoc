module Advent.Y2015.Day04 where

import Advent.Utils
import Data.Digest.Pure.MD5
import qualified Data.ByteString as BS
import qualified Data.ByteString.Char8 as BSC
import qualified Data.ByteString.Lazy as BSL (fromStrict)
import Data.Word

type Input = String

-- TODO: Paralellize, or find some other way to make it faster
getHash :: Input -> [Word8]
getHash = BS.unpack . md5DigestBytes . md5 . BSL.fromStrict . BSC.pack

find5zeros :: Input -> Int
find5zeros s = go 1
          where go n = case getHash (s ++ show n) of
                  (0 : 0 : c : _) -> if c < 8 then n else go (n + 1)
                  _               -> go (n + 1)

find6zeros :: Input -> Int
find6zeros s = go 1
          where go n = case getHash (s ++ show n) of
                  (0 : 0 : 0 : _) -> n
                  _               -> go (n + 1)

star1 :: Input -> Int
star1 = find5zeros

star2 :: Input -> Int
star2 = find6zeros

solution :: Stars -> Input -> Result
solution s i = case s of
                  First  -> pure . show . star1 $ i
                  Second -> pure . show . star2 $ i
                  Both   -> (`solution` i) =<< [First, Second]
