module Advent.Y2015.Day05 where

import Advent.Utils
import qualified Data.Set as S
import Data.List (nub, nubBy, group)

type Input = String

nice :: Input -> Bool
nice s = vowely s && repeaty s && free s

vowely :: Input -> Bool
vowely = (> 2) . length . filter (`elem` "aeiou")

repeaty :: Input -> Bool
repeaty = or . fmap ((> 1 ) . length) . group

free :: Input -> Bool
free = not . or . fmap (`elem` ["ab","cd","pq","xy"]) . pairs

pairs :: Input -> [Input]
pairs xs = zipWith (\x y -> [x,y]) xs $ drop 1 xs

doubleRepeaty :: Input -> Bool
doubleRepeaty s = let s' = zip (pairs s) [0..]
                      f  (s, n) (r, m) = s == r && abs (m - n) > 1
                  in nubBy f s' /= s'

middley :: Input -> Bool
middley s = or . fmap (\(a, b, c) -> a == c) $ zip3 s (drop 1 s) (drop 2 s)

nice2 :: Input -> Bool
nice2 s = doubleRepeaty s && middley s

star1 :: [Input] -> Int
star1 = length . filter nice

star2 :: [Input] -> Int
star2 = length . filter nice2

solution :: Stars -> Input -> Result
solution s i = case s of
                  First  -> pure . show . star1 . lines $ i
                  Second -> pure . show . star2 . lines $ i
                  Both   -> (`solution` i) =<< [First, Second]
