module Advent.Y2015.Day11 where

import Advent.Utils
import Data.List (group)

next :: Input -> Input
next [] = []
next s | ('z' : xs ) <- reverse s
                        = reverse ('a' : (reverse . next . reverse) xs)
next s | ( x : xs ) <- reverse s
                       = reverse (succ x : xs)

allPassesAfter :: Input -> [Input]
allPassesAfter = iterate next

valid1 :: (Enum a, Eq a) => [a] -> Bool
valid1 (x : x'@(y : z : xs)) = (y == succ x && z == succ y) || valid1 x'
valid1 _ = False

valid2 :: Input -> Bool
valid2 = not . any (`elem` "iol")

valid3 :: Input -> Bool
valid3 xs = let ls = fmap length . group $ xs
                twoPair = (> 1) . length . filter (> 1) $ ls
                oneQuad = any (> 3) ls
            in twoPair || oneQuad

valid :: Input -> Bool
valid s = valid1 s && valid2 s && valid3 s

star1 :: Input -> Input
star1 = head . filter valid . allPassesAfter

starBoth :: Input -> [Input]
starBoth = take 2 . filter valid . allPassesAfter

solution :: Stars -> Input -> Result
solution s i = case s of
                  First  -> pure . star1 $ i
                  Second -> drop 1 . starBoth $ i
                  Both   -> starBoth i
