module Advent.Y2015.Day22 where

import Advent.Utils
import Control.Monad.Reader

type RPG a = Reader Boss a

newtype Boss = Boss { bossDmg :: Int }

data Effect = Effect { spell :: Spell
                     , timer :: EffectState } deriving (Eq, Ord, Show)

data EffectState = New Int | Timer Int deriving (Eq, Ord, Show)

data Spell = Missile | Drain | Shield | Poison | Recharge deriving (Eq, Ord, Show)

spells :: [Spell]
spells = [Missile, Drain, Shield, Poison, Recharge]

data State = State { manaSpent   :: Int
                   , playerHP    :: Int
                   , playerArmor :: Int
                   , playerMana  :: Int
                   , bossHP      :: Int
                   , effects     :: [Effect] } deriving (Eq, Ord, Show)

manaCost :: Spell -> Int
manaCost Missile = 53
manaCost Drain = 73
manaCost Shield = 113
manaCost Poison = 173
manaCost Recharge = 229

canAfford :: State -> Spell -> Bool
canAfford State {playerMana = m, effects = es} sp = manaCost sp <= mn
            where mn = m + 101 * length (filter isRecharge es)
                  isRecharge (Effect Recharge _) = True
                  isRecharge _ = False

canCast :: State -> Spell -> Bool
canCast State {effects = es} sp = sp `notElem` active
            where active = spell <$> filter ((/= Timer 1) . timer) es

castSpell :: Spell -> State -> State
castSpell sp s@(State ms php pa pm bhp es)
            = case sp of
                Missile  -> s { manaSpent = ms + 53
                              , playerMana = subtract 53 pm
                              , bossHP = subtract 4 bhp
                              }
                Drain    -> s { manaSpent = ms + 73
                              , playerMana = subtract 73 pm
                              , playerHP = php + 2
                              , bossHP = subtract 2 bhp
                              }
                Shield   -> s { manaSpent = ms + 113
                              , playerMana = subtract 113 pm
                              , playerArmor = pa + 7
                              , effects = Effect Shield (New 6) : es
                              }
                Poison   -> s { manaSpent = ms + 173
                              , playerMana = subtract 173 pm
                              , effects = Effect Poison (New 6) : es
                              }
                Recharge -> s { manaSpent = ms + 229
                              , playerMana = subtract 229 pm
                              , effects = Effect Recharge (New 5) : es
                              }

runEffects :: State -> State
runEffects s@State { effects = es } = foldr runEffect s es

elapseEffects :: State -> State
elapseEffects s@State { effects = es, playerArmor = pa }
            = s { effects = filter ((/= Timer 0) . timer) es'
                , playerArmor = pa' }
                        where es' = f <$> es
                              pa' = if Effect Shield (Timer 0) `elem` es'
                                      then 0
                                      else pa
                              f (Effect e (New n)) = Effect e (Timer n)
                              f (Effect e (Timer n)) = Effect e (Timer $ n - 1)

runEffect :: Effect -> State -> State
runEffect (Effect e t) s@(State ms php pa pm bhp es)
       = case e of
           Shield   -> if t == Timer 0 then s { playerArmor = pa - 7 } else s
           Poison   -> s { bossHP = subtract 3 bhp }
           Recharge -> s { playerMana = pm + 101 }

playerTurn :: State -> Spell -> State
playerTurn st sp = elapseEffects . castSpell sp . runEffects $ st

bossTurn :: Int -> State -> State
bossTurn d = elapseEffects . bossHit d . runEffects

bossHit :: Int -> State -> State
bossHit _ s | bossHP s <= 0 = s
bossHit d s@State { playerHP = php, playerArmor = pa }
                      = s { playerHP = php - max 1 (d - pa) }

turn :: Int -> State -> Spell -> State
turn d st = bossTurn d . playerTurn st

turn' :: Int -> State -> Spell -> State
turn' d st sp = bossTurn d . (`playerTurn` sp) $ deduceHP st

deduceHP :: State -> State
deduceHP s@State { playerHP = php } = s { playerHP = php - 1 }

initState :: Int -> State
initState h = State { manaSpent   = 0
                    , playerHP    = 50
                    , playerArmor = 0
                    , playerMana  = 500
                    , bossHP      = h
                    , effects     = [] }

star1 :: (Int, Int) -> Int
star1 (h, d) = manaSpent . fst $ searchTree ((<= 0) . bossHP)
                     ((> 0) . playerHP)
                     (initState h)
                     0
                     (\s -> turn d s <$> filter
                                         (\sp -> canAfford s sp && canCast s sp)
                                         spells)
                     (const . manaSpent)

star2 :: (Int, Int) -> Int
star2 (h, d) = manaSpent . fst $ searchTree ((<= 0) . bossHP)
                     ((> 1) . playerHP)
                     (initState h)
                     0
                     (\s -> turn' d s <$> filter
                                         (\sp -> canAfford s sp && canCast s sp)
                                         spells)
                     (const . manaSpent)

parseInput :: Input -> (Int, Int)
parseInput = (\[a, b] -> (a, b)) . fmap (read . last . words) . lines

solution :: Stars -> Input -> Result
solution s i = case s of
                  First  -> pure . show . star1 . parseInput $ i
                  Second -> pure . show . star2 . parseInput $ i
                  Both   -> (`solution` i) =<< [First, Second]
