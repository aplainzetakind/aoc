module Advent.Y2015.Day18 where

import Advent.Utils
import Data.Array
import Data.List

parseInput :: Input -> Array Point Bool
parseInput = listArray ((0, 0), (99, 99)) . fmap (== '#')
             . concat . transpose . lines

neighbours :: Point -> [Point]
neighbours (x, y) = [ (a, b) | a <- [x - 1..x + 1], b <- [y - 1..y + 1]
                             , (a, b) /= (x, y)
                             , a >= 0, a <= 99
                             , b >= 0, b <= 99
                             ]

nextState :: Array Point Bool -> Point -> Bool
nextState a p = let ns = neighbours p
                    c  = length . filter id $ (a !) <$> ns
                    s  =  a ! p
                in (s && c == 2) || c == 3

nextState2 :: Array Point Bool -> Point -> Bool
nextState2 _ (0, 0) = True
nextState2 _ (0, 99) = True
nextState2 _ (99, 0) = True
nextState2 _ (99, 99) = True
nextState2 a p = nextState a p

updateArray :: (Array Point Bool -> Point -> Bool)
               -> Array Point Bool -> Array Point Bool
updateArray f a = listArray (bounds a) . fmap (f a) $ indices a

star1 :: Array Point Bool -> Int
star1 = length . filter id . elems . (!! 100) . iterate (updateArray nextState)

star2 :: Array Point Bool -> Int
star2 = length . filter id . elems . (!! 100) . iterate (updateArray nextState2)

solution :: Stars -> Input -> Result
solution s i = case s of
                  First  -> pure . show . star1 . parseInput $ i
                  Second -> pure . show . star2 . parseInput $ i
                  Both   -> (`solution` i) =<< [First, Second]
