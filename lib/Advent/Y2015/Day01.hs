module Advent.Y2015.Day01 where

import Advent.Utils

type Input = String

toOnes :: Input -> [Int]
toOnes = fmap ( \case '(' -> 1
                      ')' -> -1 )

star1 :: Input -> Int
star1 = sum . toOnes

star2 :: Input -> Int
star2 = length . takeWhile (/= -1) . scanl (+) 0 . toOnes

solution :: Stars -> Input -> Result
solution s i = case s of
                 First  -> pure . show . star1 $ i
                 Second -> pure . show . star2 $ i
                 Both   -> (`solution` i) =<< [First, Second]
