module Advent.Y2015.Day19 where

import Advent.Utils
import Data.Sequence (ViewL(..), ViewR(..), (><), (|>), (<|))
import qualified Data.Sequence as S
import qualified Data.Map as M
import Data.List
import Data.Tuple
import Data.Ord (Down(..))
import Control.Applicative

type Formula = (S.Seq Char, S.Seq Char)

replacements :: S.Seq Char -> Formula -> [S.Seq Char]
replacements ys (xs, xs') = go [] S.empty ys
    where go ds ps rs | S.null rs = ds
          go ds ps rs
            = let l = length xs
                  r :< rs' = S.viewl rs
                  rs'' = S.drop (S.length xs) rs
                  d = ps >< xs' >< rs''
              in if and . S.zipWith (==) xs $ rs
                   then go (d : ds) (ps |> r) rs'
                   else go ds (ps |> r) rs'

parseInput :: Input -> ([Formula], S.Seq Char)
parseInput st = let ls = lines st
                    f  = getFormulas . init . init $ ls
                    s  = S.fromList $ last ls
                in  (f, s)

getFormulas :: [Input] -> [Formula]
getFormulas = fmap ( (\[a, b] -> (a, b)) . termsUnsafe [0,2]
                                         . fmap S.fromList . words )

termsUnsafe :: [Int] -> [a] -> [a]
termsUnsafe ns = (\case Just a -> a; Nothing -> error "No parse.") . terms ns

star1 :: ([Formula], S.Seq Char) -> Int
star1 (m, i) = length . nub . concat $ replacements i <$> m

step :: [Formula] -> [S.Seq Char] -> [S.Seq Char]
step fs = nub . concat . liftA2 (flip replacements) fs

star2 :: ([Formula], S.Seq Char) -> Int
star2 (m, i) = snd . snd $ searchTree (== S.fromList "e") f i (length i, 0) fn g
                where fn s = replacements s =<< fmap swap m
                      f    = not . S.null
                      g s (_, n) = (length s, n + 1)

solution :: Stars -> Input -> Result
solution s i = case s of
                  First  -> pure . show . star1 . parseInput $ i
                  Second -> pure . show . star2 . parseInput $ i
                  Both   -> (`solution` i) =<< [First, Second]
