module Advent.Y2015.Day08 where

import Advent.Utils

litChars :: Input -> Int
litChars = length . concat . lines

countChars :: Input -> Int
countChars ( '\"' : xs ) = go 0 xs
       where go n "\"" = n
             go _ [a] = error "Unexpected end of string."
             go n ('\\' : '\\' : ys) = go (n + 1) ys
             go n ('\\' : '\"' : ys) = go (n + 1) ys
             go n ('\\' : 'x' : a : b : ys) = go (n + 1) ys
             go n s@('\\' : a : ys) = error $ ("Unrecognized escape sequence :"
                                                ++ s)
             go n (x : y : xs) = go (n + 1) (y : xs)

encodeCharCount :: Input -> Int
encodeCharCount = length . show

memChars :: Input -> Int
memChars = sum . fmap encodeCharCount . lines

star1 :: Input -> Int
star1 s = litChars s - memChars s

star2 :: Input -> Int
star2 s = memChars s - litChars s

solution :: Stars -> Input -> Result
solution s i = case s of
                  First  -> pure . show . star1 $ i
                  Second -> pure . show . star2 $ i
                  Both   -> (`solution` i) =<< [First, Second]
