module Advent.Y2015.Day24 where

import Advent.Utils
import Data.List (sortOn, sort)
import Data.Ord (Down(..))
import Debug.Trace

parseInput :: Input -> [Int]
parseInput = fmap read . lines

type List = [Int]

type Priority = (Down Int, Down Int)

type ChoiceState  = ((List, Int, Int) , List)

type ChoiceState2 = ((List, Int, Int, Int) , List)

good :: Int -> ChoiceState -> Bool
good t ((xs, y, z), ms) = null ms && y == t && z == t

good2 :: Int -> ChoiceState2 -> Bool
good2 t ((xs, y, z, w), ms) = null ms && y == t && z == t && w == t

keep :: Int -> ChoiceState -> Bool
keep t ((xs, y, z), ms) = y <= t && z <= t && sum xs <= t

keep2 :: Int -> ChoiceState2 -> Bool
keep2 t ((xs, y, z, w), ms) = y <= t && z <= t && w <= t && sum xs <= t

start :: List -> ChoiceState
start = (([], 0, 0), )

start2 :: List -> ChoiceState2
start2 = (([], 0, 0, 0), )

prune :: Int -> Maybe Priority -> ChoiceState -> Bool
prune n Nothing cs = keep n cs
prune n (Just (Down l, Down p)) cs@((xs, _, _), ns)
   = let l' = length  xs
         p' = product xs
         s' = sum     xs
     in keep n cs && (l', p') < (l, p)
                  && (l' /= l - 1 || (n - s') * p' < p)

prune2 :: Int -> Maybe Priority -> ChoiceState2 -> Bool
prune2 n Nothing cs = keep2 n cs
prune2 n (Just (Down l, Down p)) cs@((xs, _, _, _), ns)
   = let l' = length  xs
         p' = product xs
         s' = sum     xs
     in keep2 n cs && (l', p') < (l, p)
                   && (l' /= l - 1 || (n - s') * p' < p)
                   && (l' /= l - 2 || (n - s' - 1) * p' < p
                                   || (n - s') `elem` ns)

score :: Int -> ChoiceState -> Maybe Priority
score t s@((xs, _, _), _) = if good t s
                                 then Just ( Down (length xs)
                                           , Down (product xs) )
                                 else Nothing

score2 :: Int -> ChoiceState2 -> Maybe Priority
score2 t s@((xs, _, _, _), _) = if good2 t s
                                 then Just ( Down (length xs)
                                           , Down (product xs) )
                                 else Nothing

options :: ChoiceState -> [ChoiceState]
options ((xs, y, z), m : ms)
           = fmap (, ms) [ (m : xs,     y, z    )
                         , (    xs, y + m, z    )
                         , (    xs,     y, z + m) ]
options cs = []

options2 :: ChoiceState2 -> [ChoiceState2]
options2 ((xs, y, z, w), m : ms)
           = fmap (, ms) [ (m : xs,     y, z    , w    )
                         , (    xs, y + m, z    , w    )
                         , (    xs,     y, z + m, w    )
                         , (    xs,     y, z    , w + m) ]
options2 cs = []

star1 :: [Int] -> Int
star1 ns' = let t  = sum ns' `div` 3
                ns = sortOn Down ns'
            in (\(Just (_, Down c)) -> c)
               $ depthFirstPrune (score t) options (prune t) (start ns)

star2 :: [Int] -> Int
star2 ns' = let t  = sum ns' `div` 4
                ns = sortOn Down ns'
            in (\(Just (_, Down c)) -> c)
               $ depthFirstPrune (score2 t) options2 (prune2 t) (start2 ns)

solution :: Stars -> Input -> Result
solution s i = case s of
                  First  -> pure . show . star1 . parseInput $ i
                  Second -> pure . show . star2 . parseInput $ i
                  Both   -> (`solution` i) =<< [First, Second]
