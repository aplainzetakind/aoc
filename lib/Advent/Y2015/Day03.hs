module Advent.Y2015.Day03 where

import Advent.Utils
import Control.Monad.State (State(..), get, put, execState)
import Data.Foldable (traverse_)
import qualified Data.Set as S (Set, insert, singleton, size, fromList)

type Input = String

type Travel a = State (Point, S.Set Point) a
type Travel' a = State RoboState a

data Turn = Robot | Santa deriving Eq

data RoboState = RoboState { roboPos  :: Point
                           , santaPos :: Point
                           , turn     :: Turn
                           , visited  :: S.Set Point }

initState :: RoboState
initState = RoboState (0,0) (0,0) Santa $ S.singleton (0,0)

parseInput :: Input -> [Direction]
parseInput = fmap (\case
                  '>' -> R
                  '^' -> U
                  '<' -> L
                  'v' -> D)

move :: Direction -> Point -> Point
move = (`movePoint` 1)

step :: Direction -> Travel ()
step d = do (p, s) <- get
            let p' = move d p
                s' = S.insert p' s
            put (p', s')

step' :: Direction -> Travel' ()
step' d = do RoboState rp sp t v <- get
             if t == Santa
               then do let sp' = move d sp
                           v' = S.insert sp' v
                           t' = Robot
                       put $ RoboState rp sp' t' v'
               else do let rp' = move d rp
                           v' = S.insert rp' v
                           t' = Santa
                       put $ RoboState rp' sp t' v'

travel :: [Direction] -> Travel ()
travel = traverse_ step

travel' :: [Direction] -> Travel' ()
travel' = traverse_ step'

star1 :: [Direction] -> Int
star1 = S.size . snd . (`execState` ((0,0), S.fromList [(0,0)])) . travel

star2 :: [Direction] -> Int
star2 = S.size . visited . (`execState` initState) . travel'

solution :: Stars -> Input -> Result
solution s i = case s of
                  First  -> pure . show . star1 . parseInput $ i
                  Second -> pure . show . star2 . parseInput $ i
                  Both   -> (`solution` i) =<< [First, Second]
