module Advent.Y2015.Day09 where

import Advent.Utils
import Data.Ord (comparing)
import Data.List (sort, nub, permutations)
import qualified Data.Map as M
import qualified Data.Set as S

parseRoute :: Input -> (Pair Input, Int)
parseRoute l | [o, "to", d, "=", dist] <- words l = (mkPair o d, read dist)
parseRoute _ = error "parseRoute: no parse."

toInputs = (\(x , y) -> [x, y]) . pairToTuple

parseInput :: Input -> M.Map (Pair Input) Int
parseInput = M.fromList . fmap parseRoute . lines

allRoutes :: M.Map (Pair Input) Int -> [[Input]]
allRoutes = permutations . nub . concatMap toInputs . M.keys

routeLength :: M.Map (Pair Input) Int -> [Input] -> Int
routeLength m = sum . fmap ((m M.!) . pairFromTuple) . consecs
                 where consecs ss = zip ss (drop 1 ss)

star1 :: M.Map (Pair Input) Int -> Int
star1 m = minimum $ routeLength m <$> allRoutes m

star2 :: M.Map (Pair Input) Int -> Int
star2 m = maximum $ routeLength m <$> allRoutes m

solution :: Stars -> Input -> Result
solution s i = case s of
                  First  -> pure . show . star1 . parseInput $ i
                  Second -> pure . show . star2 . parseInput $ i
                  Both   -> (`solution` i) =<< [First, Second]
