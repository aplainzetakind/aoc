module Advent.Y2015.Day16 where

import Advent.Utils
import qualified Data.Map as M

type Evidence  = M.Map Input Int
type Evidence2 = M.Map Input (Int -> Bool)

pairOff :: [a] -> [(a, a)]
pairOff (x : y : xs) = (x, y) : pairOff xs
pairOff _ = []

target :: Evidence
target = M.fromList [ ("children", 3 )
                    , ("cats", 7 )
                    , ("samoyeds", 2 )
                    , ("pomeranians", 3 )
                    , ("akitas", 0 )
                    , ("vizslas", 0 )
                    , ("goldfish", 5 )
                    , ("trees", 3 )
                    , ("cars", 2 )
                    , ("perfumes", 1 )
                    ]

target2 :: Evidence2
target2 = M.fromList [ ("children", (== 3) )
                     , ("cats", (> 7) )
                     , ("samoyeds", (== 2) )
                     , ("pomeranians", (< 3) )
                     , ("akitas", (== 0) )
                     , ("vizslas", (== 0) )
                     , ("goldfish", (< 5) )
                     , ("trees", (> 3) )
                     , ("cars", (== 2) )
                     , ("perfumes", (== 1) )
                     ]

checkMatch :: (Input, Int) -> Bool
checkMatch (s, n) = n == target M.! s

checkMatch2 :: (Input, Int) -> Bool
checkMatch2 (s, n) = target2 M.! s $ n

allMatch  :: [(Input, Int)] -> Bool
allMatch = all checkMatch

allMatch2  :: [(Input, Int)] -> Bool
allMatch2 = all checkMatch2

listToSample :: [Input] -> (Int, [(Input, Int)])
listToSample s | (x : xs) <- pairOff s = (read (snd x), fmap read <$> xs)

parseLine :: Input -> (Int, [(Input, Int)])
parseLine = listToSample . words . filter (not . (`elem` ",:"))

parseInput :: Input -> [(Int, [(Input, Int)])]
parseInput = fmap parseLine . lines

star1 :: [(Int, [(Input, Int)])] -> Int
star1 = fst . head . filter snd . fmap (fmap allMatch)

star2 :: [(Int, [(Input, Int)])] -> Int
star2 = fst . head . filter snd . fmap (fmap allMatch2)

solution :: Stars -> Input -> Result
solution s i = case s of
                  First  -> pure . show . star1 . parseInput $ i
                  Second -> pure . show . star2 . parseInput $ i
                  Both   -> (`solution` i) =<< [First, Second]
