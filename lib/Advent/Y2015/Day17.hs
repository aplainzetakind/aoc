module Advent.Y2015.Day17 where

import Advent.Utils
import Data.List
import Data.Ord

combinations :: Eq a => Int -> [a] -> [[a]]
combinations n xs | n > length xs = []
combinations 0 _ = [[]]
combinations n (x : xs) = ((x :) <$> combinations (n - 1) xs)
                           ++ combinations n xs

polyMult :: [Int] -> [Int] -> [Int]
polyMult [] _ = []
polyMult _ [] = []
polyMult xs ys = let m = length xs - 1
                     n = length ys - 1
                 in [ sum [ (xs !! i) * (ys !! (d - i))
                      | i <- [0..d], i <= m, i >= d - n ]
                    | d <- [0..m + n] ]

toPoly :: Int -> [Int]
toPoly n = 1 : replicate (n - 1) 0 ++ [1]

getSums :: Int -> [Int] -> Int
getSums n ns = let res = foldr polyMult [1] . fmap toPoly $ ns
                   d   = length res - 1
               in if n > d then 0 else res !! n

parseInput :: Input -> [Int]
parseInput = fmap read . lines

star1 :: [Int] -> Int
star1 = getSums 150

star2 nums = let combs = (`combinations` nums) <$> [1..]
             in head . dropWhile (== 0) . tail . fmap (sum . fmap (getSums 150))
                $ combs

solution :: Stars -> Input -> Result
solution s i = case s of
                  First  -> pure . show . star1 . parseInput $ i
                  Second -> pure . show . star2 . parseInput $ i
                  Both   -> (`solution` i) =<< [First, Second]
