module Advent.Y2018.Day08 where

import Advent.Utils

parseInput :: Input -> a
parseInput = undefined

star1 :: a -> Int
star1 = undefined

star2 :: a -> Int
star2 = undefined

solution :: Stars -> Input -> Result
solution s i = case s of
                  First  -> pure . show . star1 . parseInput $ i
                  Second -> pure . show . star2 . parseInput $ i
                  Both   -> (`solution` i) =<< [First, Second]
