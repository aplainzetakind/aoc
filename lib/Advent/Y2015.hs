module Advent.Y2015 where

import Advent.Utils (Stars, Result, Input)
import qualified Advent.Y2015.Day01 as Day01
import qualified Advent.Y2015.Day02 as Day02
import qualified Advent.Y2015.Day03 as Day03
import qualified Advent.Y2015.Day04 as Day04
import qualified Advent.Y2015.Day05 as Day05
import qualified Advent.Y2015.Day06 as Day06
import qualified Advent.Y2015.Day07 as Day07
import qualified Advent.Y2015.Day08 as Day08
import qualified Advent.Y2015.Day09 as Day09
import qualified Advent.Y2015.Day10 as Day10
import qualified Advent.Y2015.Day11 as Day11
import qualified Advent.Y2015.Day12 as Day12
import qualified Advent.Y2015.Day13 as Day13
import qualified Advent.Y2015.Day14 as Day14
import qualified Advent.Y2015.Day15 as Day15
import qualified Advent.Y2015.Day16 as Day16
import qualified Advent.Y2015.Day17 as Day17
import qualified Advent.Y2015.Day18 as Day18
import qualified Advent.Y2015.Day19 as Day19
import qualified Advent.Y2015.Day20 as Day20
import qualified Advent.Y2015.Day21 as Day21
import qualified Advent.Y2015.Day22 as Day22
import qualified Advent.Y2015.Day23 as Day23
import qualified Advent.Y2015.Day24 as Day24
import qualified Advent.Y2015.Day25 as Day25

advent :: Int -> Stars -> Input -> Result
advent = \case
            1  -> Day01.solution
            2  -> Day02.solution
            3  -> Day03.solution
            4  -> Day04.solution
            5  -> Day05.solution
            6  -> Day06.solution
            7  -> Day07.solution
            8  -> Day08.solution
            9  -> Day09.solution
            10 -> Day10.solution
            11 -> Day11.solution
            12 -> Day12.solution
            13 -> Day13.solution
            14 -> Day14.solution
            15 -> Day15.solution
            16 -> Day16.solution
            17 -> Day17.solution
            18 -> Day18.solution
            19 -> Day19.solution
            20 -> Day20.solution
            21 -> Day21.solution
            22 -> Day22.solution
            23 -> Day23.solution
            24 -> Day24.solution
            25 -> Day25.solution
