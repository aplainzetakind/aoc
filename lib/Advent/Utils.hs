{-# LANGUAGE ConstraintKinds #-}
{-# LANGUAGE PatternSynonyms #-}
{-# LANGUAGE ViewPatterns    #-}
{-# LANGUAGE ScopedTypeVariables #-}
module Advent.Utils ( Stars(..)
                    -- , Result
                    , choose
                    , Point
                    , Direction(..)
                    , Pair
                    , mkPair
                    , movePoint
                    , pairToTuple
                    , pairFromTuple
                    , terms
                    , terms'
                    , searchTree
                    , depthFirstPrune
                    , safeHead
                    , Parser
                    , Solver
                    , InputStream
                    , T.Text((:<), (:<>))
                    , module Text.Megaparsec
                    , module Text.Megaparsec.Char
                    , module Text.Megaparsec.Char.Lexer
                    , module Data.Functor
                    , foldl'
                    , chunkToTokens'
                    , tokenToChunk'
                    , takeWhileP'
                    , takeWhile1P'
                    , dfs
                    ) where

import Data.List (foldl')
import Data.Maybe (fromJust)
import qualified Data.PSQueue as Q
import Data.PSQueue (Binding ((:->)))
import qualified Data.Set as S
import Text.Megaparsec hiding (State)
import Text.Megaparsec.Char
import Text.Megaparsec.Char.Lexer hiding (space)
import Data.Functor (void, (<$), ($>))
import Data.Void
import qualified Data.Text.Lazy as T
import Data.String
import Data.Proxy
import Data.List.Extra (firstJust)

pattern (:<) :: Char -> T.Text -> T.Text
pattern (:<) c cs <- (T.uncons -> Just (c, cs))
        where (:<) c cs = c `T.cons` cs

pattern (:<>) :: T.Text
pattern (:<>) <- (T.null -> True)
        where (:<>) = T.empty

-- Common types

type Solver a b c = (a -> (b, c))

type Parser s = Parsec Void s

type InputStream s = ( Semigroup s
                     , Tokens s ~ s
                     , IsString (Tokens s)
                     , Token s ~ Char
                     , Show s
                     , Stream s)

chunkToTokens' :: forall s. InputStream s => s -> String
chunkToTokens' = chunkToTokens (Proxy :: Proxy s)

tokenToChunk' :: forall s. InputStream s => Char -> s
tokenToChunk' = tokenToChunk (Proxy :: Proxy s)

takeWhileP' :: InputStream s => (Token s -> Bool) -> Parser s (Tokens s)
takeWhileP' = takeWhileP Nothing

takeWhile1P' :: InputStream s => (Token s -> Bool) -> Parser s (Tokens s)
takeWhile1P' = takeWhile1P Nothing

data Stars = First | Second | Both deriving Show

-- 2D
type Point = (Int, Int)
data Direction = R | U | L | D deriving Show

movePoint :: Direction -> Int -> Point -> Point
movePoint R d (x, y) = (x + d, y)
movePoint U d (x, y) = (x, y + d)
movePoint L d (x, y) = (x - d, y)
movePoint D d (x, y) = (x, y - d)

-- Combinatorial utility functions.
choose :: [a] -> Int -> [[a]]
choose _ 0 = [[]]
choose [] _ = []
choose (x : xs) n = ((x :) <$> choose xs (n - 1)) ++ choose xs n

-- 2 element sets.
data Pair a where
      Pair :: Ord a => a -> a -> Pair a

mkPair :: Ord a => a -> a -> Pair a
mkPair x y | x <= y    = Pair x y
           | otherwise = Pair y x

pairToTuple :: Pair a -> (a , a)
pairToTuple (Pair x y) = (x , y)

pairFromTuple :: Ord a => (a, a) -> Pair a
pairFromTuple (x, y) = mkPair x y

instance Eq (Pair a) where
  (==) x y = (== EQ) $ compare x y

instance Ord (Pair a) where
  compare (Pair x y) (Pair z w) = compare (x, y) (z, w)

instance Show a => Show (Pair a) where
  show (Pair x y) = unwords ["Pair", show x, show y]

-- Simple way to parse input lines.
terms :: [Int] -> [a] -> Maybe [a]
terms (n : ns) xs = case drop n xs of
                      (r : rs) -> (r :) <$> terms (subtract (n + 1) <$> ns) rs
                      _        -> Nothing
terms [] _ = Just []

terms' :: [Int] -> [a] -> [a]
terms' = (fromJust .) . terms

-- Misc

-- bind2 :: Monad m => (a -> b -> m c) -> m a -> m b -> m c
-- bind2 f ma mb = do a <- ma
                   -- b <- mb
                   -- f a b

safeHead :: [a] -> Maybe a
safeHead [] = Nothing
safeHead (x : _) = Just x

-- searchTree
searchTree :: (Ord a, Ord b)
              => (a -> Bool) -- Matching test.
              -> (a -> Bool) -- Filter.
              -> a -- Root node.
              -> b -- Root node priority.
              -> (a -> [a]) -- Function mapping a node to the list of its
                            -- neighbours.
              -> (a -> b -> b) -- Priority of new key, based on parent.
              -> (a, b) -- Match, along with the priority.
searchTree b f r p0 fn g = go S.empty $ Q.singleton r p0
    where go done queue
           = case Q.minView queue of
               Just (k :-> p, queue')
                          -> if b k
                               then (k, p)
                               else let ns = filter (\x -> S.notMember x done
                                                    && f x) $ fn k
                                        insrt ky = Q.insert ky $ g ky p
                                        queue'' = foldr insrt queue' ns
                                        done' = S.insert k done
                                    in go done' queue''
               Nothing -> error "Tree search failed."

dfs :: (a -> Bool) -> (a -> [a]) -> a -> Maybe a
dfs t cs = go
    where go n = if t n then Just n else firstJust go (cs n)

-- | Searches a tree depth first to find a node with maximal score. The score
-- has type Ord b => Maybe b, with Just results representing acceptable nodes
-- and Nothing representing no match. The score should be non-increasing with
-- respect to descending the tree.
depthFirstPrune :: (Show a, Show b, Ord a, Ord b)
                   => (a -> Maybe b) -- Scoring.
                   -> (a -> [a]) -- Children.
                   -> (Maybe b -> a -> Bool) -- Pruning test.
                   -> a -- Root.
                   -> Maybe b
depthFirstPrune s cs f r0 = go b0 r0
    where b0 = s r0
          go b r = -- if trace (show (b, r)) False then undefined
                    -- else
                    case filter (f b) (cs r) of
                          [] -> b
                          ns -> foldl' search b ns
                                  where search mn n = go (max mn $ s n) n


-- searchTreeM :: (Monad m, Ord a, Ord b)
               -- => (a -> m Bool) -- Matching test.
               -- -> (a -> m Bool) -- Filter.
               -- -> m a -- Root node.
               -- -> m b -- Root node priority.
               -- -> (a -> m [a]) -- Function mapping a node to the list of its
                             -- -- neighbours.
               -- -> (a -> b -> m b) -- Priority of new key, based on parent.
               -- -> m (a, b) -- Match, along with the priority.
-- searchTreeM b f r p0 fn g = do r_ <- r
                               -- p_ <- p0
                               -- go S.empty $ Q.singleton r_ p_
    -- where go done queue
           -- = case Q.minView queue of
               -- Just (k :-> p, queue')
                 -- -> ifM (b k) (pure (k, p))
                      -- $ do ns <- filterM (\x -> (S.notMember x done
                                                  -- &&) <$> f x) =<< fn k
                           -- let done'    = S.insert k done
                               -- insrt ky q = (\p -> Q.insert ky p q) <$> g ky p
                           -- queue'' <- foldrM insrt queue' ns
                           -- go done' queue''
